// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var webserver = require('gulp-webserver');

// Concatenate & Minify JS
gulp.task('vendor', function(){
    return gulp.src([
        "app/bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js",
        "app/bower_components/jquery/dist/jquery.min.js",
        "app/bower_components/angular/angular.js",
        "app/bower_components/angular-route/angular-route.js",
        "app/bower_components/angular-animate/angular-animate.js",
        "app/bower_components/angular-sanitize/angular-sanitize.js",
        "app/bower_components/angular-touch/angular-touch.js",
        "app/bower_components/angular-resource/angular-resource.js",
        "app/bower_components/angular-cookies/angular-cookies.js",
        "app/bower_components/angular-translate/angular-translate.js",
        "app/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js",
        "app/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js",
        "app/bower_components/angular-translate-storage-local/angular-translate-storage-local.js",
        "app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
        "app/bower_components/angular-ui-utils/ui-utils.js",
        "app/bower_components/ngstorage/ngStorage.js",
        "app/bower_components/moment/moment.js",
        "app/bower_components/bootstrap-daterangepicker/daterangepicker.js",
        "app/bower_components/ng-bs-daterangepicker/src/ng-bs-daterangepicker.js",
        "app/bower_components/chosen/chosen.jquery.min.js",
        "app/bower_components/highcharts/highcharts.js",
        "app/bower_components/highcharts/modules/exporting.js"
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});
gulp.task('scripts', function() {
    return gulp.src([
        "app/app.js",
        "app/app.config.js",
        "app/views/dashboard/Dashboard.config.js",
        "app/views/consumptions/Consumptions.config.js",
        "app/views/diagnosis/Diagnosis.config.js",
        "app/views/budget/Budget.config.js",
        "app/views/performance/Performance.config.js",
        "app/views/dss/Dss.config.js",
        "app/views/input/Input.config.js",
        "app/views/login/Login.config.js",
		"app/views/logout/Logout.config.js",

        "app/controllers/MainController.js",
        "app/views/input/InputController.js",
        "app/views/login/LoginController.js",
		"app/views/logout/LogoutController.js",

        "app/directives/ui-jq.js",
        "app/directives/ui-nav.js",
        "app/directives/ui-toggleclass.js",
        "app/directives/settings-multiple-selection.js",
        "app/directives/settings-building-selection.js",
        "app/directives/settings-cluster-selection.js",
        "app/directives/settings-city-selection.js",
        "app/directives/consumptions-chart.js",
        "app/directives/consumptions-monthly-chart.js",
        "app/directives/budget-chart.js",
        "app/directives/profiles-chart.js",
        "app/directives/building-info.js",
        "app/directives/epi-form/epi-form.js",
        "app/directives/todayconsumptions-chart.js",
        "app/directives/weather-now.js",

        "app/services/ui-load.js",
        "app/services/CreemSettings.js",
        "app/services/LoadingFactory.js",
        "app/services/BuildingsFactory.js",
        "app/services/ConsumptionsFactory.js",
        "app/services/MonthlyConsumptionsFactory.js",
        "app/services/BudgetFactory.js",
        "app/services/ProfilesFactory.js",
        "app/services/EpiFormFactory.js",
        "app/services/LoginService.js"
        ])
        .pipe(concat('application.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('application.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('copyfiles_images', function() {
    gulp.src('app/styles/images/**/*.{jpg,png,gif,jpeg}')
    .pipe(gulp.dest('public/styles/images'));
});

gulp.task('copyfiles_views', function() {
    gulp.src('app/views/**/*.{html,htm}')
    .pipe(gulp.dest('public/views'));
});

gulp.task('copyfiles_directives', function() {
    gulp.src('app/directives/**/*.{html,htm}')
    .pipe(gulp.dest('public/directives'));
});

gulp.task('copyfiles_lang', function() {
    gulp.src('app/l10n/**/*.{js,html}')
    .pipe(gulp.dest('public/l10n'));
});


gulp.task('copyfiles_fonts', function() {
    gulp.src('app/styles/fonts/**/*.{eot,woff,woff2,ttf}')
    .pipe(gulp.dest('public/styles/fonts'));
});

gulp.task('copy_index', function() {
    gulp.src('app/dist/**/*.{html,htm}')
    .pipe(gulp.dest('public/'));
});

gulp.task('stylesheets', function() {
    return gulp.src(['app/bower_components/html5-boilerplate/dist/css/normalize.css',
        'app/bower_components/html5-boilerplate/dist/css/main.css',
        'app/bower_components/bootstrap/dist/css/bootstrap.css',
        'app/bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css',
        'app/bower_components/chosen/chosen.css',
        'app/styles/**/*.css' 
    ])
    .pipe(concat('stylesheets.css'))
    .pipe(gulp.dest('public/styles/css'));
});


gulp.task('webserver', function() {
    gulp.src('.')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: true
        }));
});

// Default Task
gulp.task('default', ['scripts', 'vendor', 'stylesheets', 'copyfiles_images', 'copyfiles_views', 'copyfiles_lang', 'copyfiles_fonts', 'copyfiles_directives', 'copy_index']);