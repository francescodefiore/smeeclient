'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/import', {
        templateUrl: 'views/import/ImportView.html',
        controller: 'ImportController'
    });
}]);