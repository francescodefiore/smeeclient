'use strict';

CREEMapp.config(['$routeProvider', 

function($routeProvider) 
{

  $routeProvider
  
	.when('/consumptions/historic', {
		templateUrl: 'views/consumptions/HistoricalConsumptionsView.html'
	})
	
	.when('/consumptions/monthly', {
		templateUrl: 'views/consumptions/MonthlyConsumptionsView.html'
	})
	
	.when('/consumptions/profiles', {
		templateUrl: 'views/consumptions/ProfileConsumptionsView.html'
	})
	
	.when('/consumptions/coalbalance', {
		templateUrl: 'views/consumptions/CoalBalanceView.html'
	})
	
	.when('/consumptions/busdata', {
		templateUrl: 'views/consumptions/BusDataView.html'
	})
	.when('/consumptions/acoteldata', {
		templateUrl: 'views/consumptions/AcotelView.html'
	});
	
  
  
  
  ;
  
}]);