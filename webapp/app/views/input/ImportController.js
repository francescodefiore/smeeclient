CREEMapp.controller('ImportController', ['$scope', 'FileUploader', 'RESTservAddr', function ($scope, FileUploader, RESTservAddr) {
    $scope.uploadTypes = [
        {name: 'Budget', url: RESTservAddr + '/Budget/upload'},
        {name: 'Consumi', url: RESTservAddr + '/consumi/upload'},
        {name: 'Consuntivi', url: RESTservAddr + '/consuntivi/upload'},
		{name: 'Acotel', url: RESTservAddr + '/acotel/upload'}
    ];
    $scope.uploadType = $scope.uploadTypes[0];
    $scope.uploadTypeChange = function () {
        $scope.uploader.url = $scope.uploadType.url;
		$scope.uploadType = $scope.uploadType;
    } 
	$scope.uploader = new FileUploader({
		url: $scope.uploadType.url
    });
}])