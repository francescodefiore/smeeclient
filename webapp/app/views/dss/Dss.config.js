'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/dss', {
    templateUrl: 'views/dss/DssView.html'
  })
   .when('/dss/manutenzione', {
		templateUrl: 'views/dss/ManutenzioneOrdinariaView.html'
	})
	.when('/dss/scenari/manutenzione', {
		templateUrl: 'views/dss/ManutenzioneStraordinariaView.html'
	})
	
	.when('/dss/ticket', {
		templateUrl: 'views/dss/TicketView.html'
	})
	
	.when('/dss/ticketSchedulati', {
		templateUrl: 'views/dss/TicketSchedulatiView.html'
	})
}]);