'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/logout', {
        templateUrl: 'views/logout/LogoutView.html'
    });
}]);