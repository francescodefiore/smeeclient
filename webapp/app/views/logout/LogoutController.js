'use strict';

CREEMapp.controller('LogoutController', ['$scope', 'LoginService','$location',

	function ($scope, LoginService, $location) 
	{
	
			$scope.logout = function()
			{								
				LoginService.signOut();
			}
			
			$scope.back_dashboard = function()
			{								
				$location.path('/dashboard');
			}
			
	}
	
	
]);