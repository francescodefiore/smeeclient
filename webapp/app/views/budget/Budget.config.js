'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/budget', {
    templateUrl: 'views/budget/BudgetView.html'
  });
}]);