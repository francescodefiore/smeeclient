'use strict';

var CREEMapp = angular.module('creem', [
				  'ngRoute',
				  'ngAnimate',
				  'ngCookies',
				  'ngResource',
				  'ngSanitize',
				  'ngTouch',
				  'ngBootstrap',
				  'ngStorage',
				  'pascalprecht.translate',
				  'ui.bootstrap',
				  'angularFileUpload',
				  'leaflet-directive',
				  'ngTable',
				  'adaptv.adaptStrap',
				  'ui.calendar',
				  'ui.bootstrap.datetimepicker'
]);