CREEMapp.directive('energyChart', 

function() 
{

	return {
    scope: {},
    controller: ['$rootScope', '$scope', 'CreemSettings', 'BusDataFactory', 
	
		function($rootScope, $scope, CreemSettings, BusDataFactory)
		{			
			var seriesCounter = 0;
			$scope.data = [];
			$scope.energydata = [];
			$scope.energyseries = {};
						
			
			var createChart = function(){
				var today = moment().format("dddd, MMMM Do YYYY");	
				var histdaticampochart = new Highcharts.Chart({
					chart: {
						renderTo: 'histenergycampo',
						type: 'spline',
						zoomType: 'x'
					},
					title: {
						text: 'Energy Meter - ' + today
					},
					
					credits: {
						enabled: false
					},
					
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: { // don't display the dummy year
							month: '%e. %b',
							year: '%b'
						},
						title: {
							text: 'Date'
						}
					},
					yAxis: [{ // Primary yAxis
								labels: {
									format: '{value}kW',
									style: {
										color: Highcharts.getOptions().colors[0]
									}
								},
								title: {
									text: 'Potenza Attiva'
								}
							}],

					plotOptions: {
						spline: {
							marker: {
								enabled: false
							}
						}
					},

					series: $scope.data
				});
			}
			
			

			var getdata = function (building) {
					$scope.energydata = BusDataFactory.getEnergyData(building.idImmobile).query(
						function() {
						
									for (var i = 0; i < $scope.energydata.length; i++) 
									{
											$scope.energyseries[$scope.energydata[i]['ipFisico']] = 
											{
												name: "EM_" + $scope.energydata[i]['ipFisico'],
												data: []
											}
									}
									
									
									for (var i = 0; i < $scope.energydata.length; i++) 
									{
											var newDate = new Date($scope.energydata[i]['timestamp']);
											
											$scope.energyseries[$scope.energydata[i]['ipFisico']].data.push
											(
												[											
													Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), 0, 0),
													parseInt($scope.energydata[i].valore)
												]
											);
									}
									
									
									for (var ipFisico in $scope.energyseries)
										$scope.data.push($scope.energyseries[ipFisico]);
										
									console.log($scope.data);
									
									
								createChart();
								
							
						}
					);
				
			}

			
			var refreshData = function ()
			{										
					$scope.energydata = [];
					$scope.data = [];
					$scope.energyseries = {};
					
					getdata(CreemSettings.selectedbuildings[0]);					
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


			
		}],
		
		templateUrl: 'directives/energycampo-chart.html'
  };
  
});
	
	