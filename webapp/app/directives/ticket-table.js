CREEMapp.directive('ticketTable', 
function() 
{
	return {    
			templateUrl: 'directives/ticket-table.html',
			
			controller: ['$scope', 'ManutenzioneFactory', 'ngTableParams', 'LoadingFactory', 'CreemSettings',
			
			function($scope, ManutenzioneFactory, ngTableParams, LoadingFactory, CreemSettings ) 
			{					
				var m_tickets = [];
				var filter_codice = "";
				var filter_dataRichiesta = "";
				var filter_ticket = "";
				
				if(CreemSettings.selectedbuildings.length > 0)					
						filter_codice = CreemSettings.selectedbuildings[0].codice;

				var createTableTicket = function(cod, time, ticket)
				{
					$scope.tableticket = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							 filter: {
								codiceImmobile: cod,
								cdIntervento: ticket,
								dataRichiesta: time
							  }
							}, {
								data: data
					});		
					
				}
	
				var data =  ManutenzioneFactory.getTicketDaSchedulare().query(
					function() 
					{		
						createTableTicket(filter_codice, filter_dataRichiesta, filter_ticket);
					}
				);
				
				$scope.colsfields = [
					//{ field: "codiceImmobile", title: "Codice", filter: { codiceImmobile: "text" }, show: true , sortable:"'codiceImmobile'"},
					{ field: "cdIntervento", title: "N. Ticket", filter: { cdIntervento: "text" }, sortable:"'cdIntervento'", show: true},
					//{ field: "buildingManager", title: "Building Manager", filter: { buildingManager: "text" }, sortable:"'buildingManager'", show: true},
					{ field: "utenteInserimento", title: "Utente", filter: { utenteInserimento: "text" }, sortable:"'utenteInserimento'", show: true},
					{ field: "descrizione", title: "Descrizione"},					
					//{ field: "dataInizioInterveto", title: "Data Inizio Intervento", filter: { dataInizioInterveto: "text" }, sortable:"'dataInizioInterveto'", show: true},
					//{ field: "dataLimite", title: "Data Limite"},
					{ field: "dataRichiesta", title: "Data Richiesta"},
					{ field: "durata", title: "Durata (gg)"},
					{field: "priorita", title: "Priorita'"}
					
				];
				
				$scope.$on('settingsUpdated', function (event, data) { 
					
					if(CreemSettings.selectedbuildings.length > 0)					
						createTableTicket(CreemSettings.selectedbuildings[0].codice, "", "", "");
				} );

		}],

  };
 
});