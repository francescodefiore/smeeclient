CREEMapp.directive('boardSelection', function() {

	  return {		
		
		templateUrl: 'directives/datiDiCampo/board-selection.html',
		scope:{tipoScheda : '=tipoScheda',
			   schedeTemp : '=schede',
			   select_board : '=select_board',
			   numeroIstanze : '=numeroIstanze'
			  },	
		controller: ['$scope', 'BusDataFactory', 'CreemSettings', function($scope, BusDataFactory, CreemSettings) {
							
							$scope.selected_building;
							$scope.schedeTemp = {};
							$scope.changeBuilding = 0;
							
							var refreshData = function ()
							{
								var scheda = [];
								$scope.schedeTemp = {};
								var temp = BusDataFactory.getBuildingBoards(CreemSettings.selectedbuildings[0].idImmobile,$scope.tipoScheda).query(
									function()
									{
										for(var i=0; i<temp.length; i++)
										{									
											scheda.push(temp[i]);
										}
										
										if(temp.length == 0)
											scheda.push('');
										
										$scope.schedeTemp = angular.copy(scheda);
										//if($scope.selected_board == null || $scope.selected_board == "")
										if($scope.changeBuilding == 1 || $scope.selected_board == null)
										{
											$scope.changeBuilding = 0;
											$scope.selected_board = $scope.schedeTemp[0];
											$scope.setSchede();
										}
										
										CreemSettings.setBoards($scope.numeroIstanze);
									}
								);
								
								
							}
							
							$scope.setSchede = function()
							{
								if($scope.tipoScheda == 3)
									BusDataFactory.addPesb($scope.selected_board);
								else if($scope.tipoScheda == 4)
									BusDataFactory.addEnergyMeter($scope.selected_board);
								if($scope.tipoScheda == 5)
									BusDataFactory.addThermalMeter($scope.selected_board);
							}
							
							$scope.update = function()
							{
								$scope.setSchede();
							}
							
							
							$scope.$on('settingsUpdated', function (event, data){
								$scope.changeBuilding = 1;
								refreshData(); 
								});
				
							if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
								refreshData();
						}
		],
		
		link: function(scope, element)
			  { 		
					var nameElement = '#board_list_' + scope.tipoScheda;
					$(element).find(nameElement).val(scope.select_board);
					scope.$watch('schedeTemp', function(newValue, oldValue) {																	
										if (newValue.length != 0)																				
										{
											$(element).find(nameElement).trigger("chosen:updated");
											$(element).find(nameElement).chosen();
										}
										
								}, true);
								
				}
				
		
		
	  }
	});
	
	