CREEMapp.directive('graficaCampo', 

function() 
{

	return {
    scope: {},
    controller: ['$rootScope', '$scope', 'CreemSettings', 'BusDataFactory',
		
		function($rootScope, $scope, CreemSettings, BusDataFactory)
		{			
			var seriesCounter = 0;
			$scope.data = [];
			$scope.pesb = {};
			$scope.energy = {};
			$scope.thermal = {};
			$scope.flagP = 0;
			$scope.flagE = 0;
			$scope.flagT = 0;
			$scope.oldBuilding = null;
			$scope.oldData = [];
			$scope.oldSelect = [
			{tipo:"PESB",key:"-1",colonna:0},
			{tipo:"ENERGY",key:"-1",colonna:0},
			{tipo:"THERMAL",key:"-1",colonna:0}
			];
			
			$scope.tipoDatoPesb =[
			{nome: "Temperatura", valore:"1"},
			{nome: "Luminosità", valore:"2"},
			{nome: "Presenza", valore:"3"},
			{nome: "Umidità", valore:"4"}
			];
			
			$scope.tipoDatoEnergyMeter =[
			{nome: "En. Attiva Esp. R", valore:"exported_active_energy_r"},
			{nome: "En. Attiva Esp. S", valore:"exported_active_energy_s"},
			{nome: "En. Attiva Esp. T", valore:"exported_active_energy_t"},
			{nome: "En. Attiva Imp. R", valore:"imported_active_energy_r"},
			{nome: "En. Attiva Imp. S", valore:"imported_active_energy_s"},
			{nome: "En. Attiva Imp. T", valore:"imported_active_energy_t"},
			{nome: "En. Reattiva Imp. R", valore:"imported_reactive_energy_r"},
			{nome: "En. Reattiva Imp. S", valore:"imported_reactive_energy_s"},
			{nome: "En. Reattiva Imp. T", valore:"imported_reactive_energy_t"},
			{nome: "En. Reattiva Esp. R", valore:"exported_reactive_energy_r"},
			{nome: "En. Reattiva Esp. S", valore:"exported_reactive_energy_s"},
			{nome: "En. Reattiva Esp. T", valore:"exported_reactive_energy_t"},
			{nome: "Tens. di Linea R", valore:"line_voltage_r"},
			{nome: "Tens. di Linea S", valore:"line_voltage_s"},
			{nome: "Tens. di Linea T", valore:"line_voltage_t"},
			{nome: "Corr. di Linea R", valore:"line_current_r"},
			{nome: "Corr. di Linea S", valore:"line_current_s"},
			{nome: "Corr. di Linea T", valore:"line_current_t"},
			{nome: "Fattore Potenza R", valore:"power_factor_r"},
			{nome: "Fattore Potenza S", valore:"power_factor_s"},
			{nome: "Fattore Potenza T", valore:"power_factor_t"},
			{nome: "Pot. Attiva R", valore:"active_power_r"},
			{nome: "Pot. Attiva S", valore:"active_power_s"},
			{nome: "Pot. Attiva T", valore:"active_power_t"},
			{nome: "Pot. Reattiva R", valore:"reactive_power_r"},
			{nome: "Pot. Reattiva S", valore:"reactive_power_s"},
			{nome: "Pot. Reattiva T", valore:"reactive_power_t"},
			{nome: "Frequenza R", valore:"frequency_r"},
			{nome: "Frequenza S", valore:"frequency_s"},
			{nome: "Frequenza T", valore:"frequency_t"},
			{nome: "Cosphi R", valore:"cosphiR"},
			{nome: "Cosphi S", valore:"cosphiS"},
			{nome: "Cosphi T", valore:"cosphiT"}
			];
			
			$scope.tipoDatoThermalMeter =[
			{nome: "Calorie", valore:"val"}
			];
			
			$scope.ranges = {							
				// CREEM: L'anno corrente è il precedente								
				'Ieri': [moment().subtract(1, 'days'), moment().subtract(1,'days')],
				'Ultima settimana': [moment().subtract(7, 'days'), moment().subtract(1,'days')],
				'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()]
			};
			
			$scope.selected_datipesb = $scope.tipoDatoPesb[0];
			$scope.selected_datienergy = $scope.tipoDatoEnergyMeter[0];
			$scope.selected_datithermal = $scope.tipoDatoThermalMeter[0];
			$scope.selected_dates = {startDate: $scope.ranges['Ieri'][0], endDate: $scope.ranges['Ieri'][1]};
			
			var createChart = function(){
				var range = "da " + formatDate($scope.selected_dates.startDate) + " a " + formatDate($scope.selected_dates.endDate);
				
				var histdaticampochart = new Highcharts.Chart({
					chart: {
						renderTo: 'histdaticampo',
						type: 'spline',
						zoomType: 'x'
					},
					title: {
						text: 'Sensori di campo - ' + range
					},
					
					credits: {
						enabled: false
					},
					
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: { // don't display the dummy year
							month: '%e. %b',
							year: '%b'
						},
						title: {
							text: 'Date'
						}
					},
					yAxis: [{ // Primary yAxis
								labels: {
									format: '{value}°C',
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[0]
									}
								},
								title: {
									text: ''
								},
								min: 0
							},
							{ 
								labels: {
									format: '{value}uv',
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[0]
									}
								},
								title: {
									text: '',
									style: {
											color: Highcharts.getOptions().colors[2]
										}
								},
								opposite: true
							},
							{ 
								labels: {
									format: '{value}%',
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[0]
									}
								},
								title: {
									text: ''
								},
								opposite: true
							},
							{ 
								labels: {
									//format: '{value}kWh',
									formatter: function () {
											 return formatterNumber(this.value,'E');
										
									},
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[1]
									}
								},
								title: {
									text: ''
								}
							},
							
							{ 
								labels: {
									//format: '{value}kWh',
									formatter: function () {
											 return formatterNumber(this.value,'T');
										
									},
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[2]
									}
								},
								title: {
									text: ''
								}
							},
							{ 
								labels: {
									enabled: false									
								},
								title: {
									enabled: false									
								}
							}
							
							
							],

					plotOptions: {
					
						spline: {
							marker: {
								enabled: false
							}
						}
					},

					series: $scope.data
				});
			}
			
			
			var formatterNumber = function(value,type)
			{
				var measureUnit = '';
				if ((value > 999 && value < 999999) || (value < -999 && value > -999999)) {
				 if(type == 'T')
					measureUnit = 'kC';
				else
					measureUnit = 'kW';
				   value = value / 1000;
				  }
				if ((value > 999999 && value < 999999999) || (value < -999999 && value > -999999999)) {
					if(type == 'T')
						measureUnit = 'MC';
					else
						measureUnit = 'MW';
				   value = value / 1000000;}
				 
				 
				//var valueTxt = value.toFixed(2);
				//var components = valueTxt.toString().split(".");
				//components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				//return components.join(",") + measureUnit;
				return value + measureUnit;
			}

			var getdata = function (building) 
			{
				
				var q = '';
				preparePesbData();
				prepareEnergyData();
				prepareThermalData();
				if($scope.flagP == 1)
					q+='PESB,';
					
				if($scope.flagE == 1)
					q+='ENERGY,';
				
				if($scope.flagT == 1)
					q+='THERMAL';
				
				if(q != '')
				{
				
					var dati = BusDataFactory.getBoardsData($scope.pesb,$scope.energy,$scope.thermal,q).query(
					function()
					{
						var d = [];
						
						if(q.indexOf('PESB') > -1)
						{
							d[0] = dati[0];
							$scope.oldData[0] = dati[0];
						}
						else
							d[0] = $scope.oldData[0];
						
						if(q.indexOf('ENERGY') > -1)
						{
							d[1] = dati[1];
							$scope.oldData[1] = dati[1];
						}
						else
							d[1] = $scope.oldData[1];
						
						if(q.indexOf('THERMAL') > -1)
						{
							d[2] = dati[2];
							$scope.oldData[2] = dati[2];
						}
						else
							d[2] = $scope.oldData[2];			
						
						if(q.indexOf('PESB,ENERGY,THERMAL') > -1)
							$scope.oldData = dati;
						
						configChart(d);
					});
				}
				
			}
			
			var configChart = function(dati)
			{
				//PESB
					//if($scope.pesb[0] != null)
						$scope.data.push({
									name: $scope.pesb.fenomeno,
									data: prepareData(dati[0]),
									yAxis: $scope.pesb.asse,
									type: $scope.pesb.colonna,
									text: $scope.pesb.colonnaNome
								});
					//ENERGY METER	
					//if($scope.energy[0] != null)					
						$scope.data.push({
									name: $scope.energy.colonnaNome,
									data: prepareData(dati[1]),
									yAxis: $scope.energy.asse,
									dashStyle: 'shortdot',
									text: $scope.energy.colonnaNome,
									type: $scope.energy.colonna
								});
					//THERMAL METER	
					//if($scope.thermal[0] != null)
						$scope.data.push({
									name: $scope.thermal.colonnaNome,
									data: prepareData(dati[2]),
									yAxis: $scope.thermal.asse,
									dashStyle: 'Dash',
									text: $scope.thermal.colonnaNome,
									type: $scope.thermal.colonna
								});
					
					createChart();
			}
			
			$scope.changeColPesb = function()
			{
				$scope.oldSelect[0].colonna = 1;
			}
			
			$scope.changeColEnergy = function()
			{
				$scope.oldSelect[1].colonna = 1;
			}
			
			$scope.changeColThermal = function()
			{
				$scope.oldSelect[2].colonna = 1;
			}
			
			$scope.changeDate = function()
			{
				$scope.oldData = [];
				$scope.oldSelect[0].colonna = 0;
				$scope.oldSelect[1].colonna = 0;
				$scope.oldSelect[2].colonna = 0;
			}
			
			var preparePesbData = function()
			{
				if(BusDataFactory.getPesb()[0] == null || BusDataFactory.getPesb()[0] == '')
				{
					$scope.flagP = 1;
					return {};
				}
				
				var index = BusDataFactory.getPesb()[0].index;
				var ipLocazione = BusDataFactory.getPesb()[0].ipS;
				var schedaPadreFenomeno = BusDataFactory.getPesb()[0].ip;
				var vSchedaPare = schedaPadreFenomeno.split(".");
				var valoreFenomeno = $scope.selected_datipesb.valore;
				var schedaFenomeno = valoreFenomeno + "." + vSchedaPare[1] + "." + vSchedaPare[2] + "." + vSchedaPare[3];
				var da = formatDate($scope.selected_dates.startDate);
				var a = formatDate($scope.selected_dates.endDate);
				var asse = -1;
				var column = "spline";
				
				if(valoreFenomeno == 1)
					asse = 0;
				else if(valoreFenomeno == 2)
					asse = 1;
				else if(valoreFenomeno == 3)
					asse = 2;
				else if(valoreFenomeno == 4)
					asse = 5;
				
				$scope.pesb = {
					
						ipLocazione: ipLocazione.toString(),
						schedaFenomeno: schedaFenomeno.toString(),
						da: da.toString(),
						a: a.toString(),
						asse : asse,
						fenomeno: $scope.selected_datipesb.nome.toString(),
						colonna: column.toString(),
						colonnaNome: $scope.selected_datipesb.nome.toString(),
					
				};
				
				var key = index + '-' + ipLocazione;
				
				if($scope.oldData.length != 0) //PER LA PRIMA VOLTA QUANDO NON HO DATI
				{
					if($scope.oldSelect[0].key != key)
					{
						$scope.oldSelect[0].key = key;
						$scope.flagP = 1;
					}
					else
						if($scope.oldSelect[0].colonna == 1)
						{
							$scope.oldSelect[0].colonna = 0;
							$scope.flagP = 1;
						}
						else
							$scope.flagP = 0;
				}
				else
					$scope.flagP = 1;
				
			}
			
			var prepareEnergyData = function()
			{
				if(BusDataFactory.getEnergyMeter()[0] == null || BusDataFactory.getEnergyMeter()[0] == '')
				{
					$scope.flagE = 1;
					return {};
				}
				
				var index = BusDataFactory.getEnergyMeter()[0].index;
				var ipLocazione = BusDataFactory.getEnergyMeter()[0].ipS;
				var ipSchedaEnergy = BusDataFactory.getEnergyMeter()[0].ip;
				var vipSchedaEnergy = ipSchedaEnergy.split(".");
				var da = formatDate($scope.selected_dates.startDate);
				var a = formatDate($scope.selected_dates.endDate);
				var asse = 3;
				$scope.energy = {
					ipLocazione: ipLocazione.toString(),
					ipSchedaEnergy: vipSchedaEnergy[1] + "." + vipSchedaEnergy[2] + "." + vipSchedaEnergy[3],
					da: da.toString(),
					a: a.toString(),
					asse : asse,
					colonnaDb: $scope.selected_datienergy.valore.toString(),
					colonnaNome: $scope.selected_datienergy.nome.toString(),
					colonna: 'spline'
				}
				
				var key = index + '-' + ipLocazione;
				
				if($scope.oldData.length != 0) //PER LA PRIMA VOLTA QUANDO NON HO DATI
				{
					if($scope.oldSelect[1].key != key)
					{
						$scope.oldSelect[1].key = key;
						$scope.flagE = 1;
					}
					else
						if($scope.oldSelect[1].colonna == 1)
						{
							$scope.oldSelect[1].colonna = 0;
							$scope.flagE = 1;
						}
						else
							$scope.flagE = 0;
				}
				else
					$scope.flagE = 1;
			
			}
			
			var prepareThermalData = function()
			{
				if(BusDataFactory.getThermalMeter()[0] == null || BusDataFactory.getThermalMeter()[0] == '')
				{
					$scope.flagT = 1;
					return {};
				}
				
				var index = BusDataFactory.getThermalMeter()[0].index;
				var ipLocazione = BusDataFactory.getThermalMeter()[0].ipS;
				var ipSchedaThermal = BusDataFactory.getThermalMeter()[0].ip;
				var vipSchedaThermal = ipSchedaThermal.split(".");
				var da = formatDate($scope.selected_dates.startDate);
				var a = formatDate($scope.selected_dates.endDate);
				var asse = 4;
				$scope.thermal = {
					ipLocazione: ipLocazione.toString(),
					ipSchedaEnergy: vipSchedaThermal[1] + "." + vipSchedaThermal[2] + "." + vipSchedaThermal[3],
					da: da.toString(),
					a: a.toString(),
					asse : asse,
					colonnaDb: $scope.selected_datithermal.valore.toString(),
					colonnaNome: $scope.selected_datithermal.nome.toString(),
					colonna: 'spline'
				}
				
				var key = index + '-' + ipLocazione;
				
				if($scope.oldData.length != 0) //PER LA PRIMA VOLTA QUANDO NON HO DATI
				{
					if($scope.oldSelect[2].key != key)
					{
						$scope.oldSelect[2].key = key;
						$scope.flagT = 1;
					}
					else
						if($scope.oldSelect[2].colonna == 1)
						{
							$scope.oldSelect[2].colonna = 0;
							$scope.flagT = 1;
						}
						else
							$scope.flagT = 0;
				}
				else
					$scope.flagT = 1;
			
			}
			
			var formatDate = function(data) {
					var d = new Date(data);
					var yyyy = d.getFullYear().toString();
					var mm = (d.getMonth()+1).toString(); // getMonth() is zero-based
					var dd  = d.getDate().toString();
					return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
					  }
			
			var prepareData = function (response)
			{				
				var tot = 0;
				var chartData = [];
				if(response[0] == null)
					return [];
				//for (var i = 0; i < response.length; i++) {
				for(var i in response){
						if(response[i].data != null && response[i].valore !=null)
						{
							var newDate = new Date(response[i].data);
							chartData.push([
								Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), newDate.getMilliseconds()),
								parseInt(response[i].valore)
							]);
						}
				}				
				return chartData;
			}
			
			
			
			var refreshData = function ()
			{	
				if($scope.oldBuilding == null)
					$scope.oldBuilding = CreemSettings.selectedbuildings[0];
					
				if($scope.oldBuilding.idImmobile != CreemSettings.selectedbuildings[0].idImmobile)
				{
					$scope.oldBuilding = CreemSettings.selectedbuildings[0]
					$scope.oldData = [];
					$scope.oldSelect = [
					{tipo:"PESB",key:"-1",colonna:0},
					{tipo:"ENERGY",key:"-1",colonna:0},
					{tipo:"THERMAL",key:"-1",colonna:0}
					];
				}
				
				$scope.data = [];
				$scope.pesb = {};
				$scope.energy = {};
				$scope.thermal = {};
					
				getdata(CreemSettings.selectedbuildings[0]);
					
			}
			
			$scope.$on('setBoards', function (event, data){
					refreshData(); 
			});
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


			
		}],
		
		templateUrl: 'directives/datiDiCampo/graficacampo-chart.html'
  };
  
});
	
	