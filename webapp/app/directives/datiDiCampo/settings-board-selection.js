CREEMapp.directive('settingsBoardSelection', function() {

	  return {		
		templateUrl: 'directives/datiDiCampo/settings-board-selection.html'	,				
		controller: ['$scope', 'BusDataFactory', 'CreemSettings', function($scope, BusDataFactory, CreemSettings) {
			
			$scope.updateChart = function()
			{
				CreemSettings.setBoards(0);
			}
		}]
		
	  }
	});
	
	