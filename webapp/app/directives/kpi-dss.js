CREEMapp.directive('kpiDss', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'KPIFactory', 'TooltipDSS', 'BuildingsFactory',
	
		function($scope, CreemSettings, KPIFactory, TooltipDSS, BuildingsFactory) 
		{
			var seriesCounter = 0;
			$scope.dataset = [];			
		    $scope.kpichart = {};
			
			$scope.selected_cluster = "ALL";
			$scope.setCluster = function() 
			{													
				CreemSettings.setCluster($scope.selected_cluster);
			}
			
			$scope.selected_superficieDss = "ALL";
			$scope.setSuperficieDss = function() 
			{													
				CreemSettings.setSuperficieDss($scope.selected_superficieDss);
			}
			
			$scope.ranges = {							
				// CREEM: L'anno corrente è il precedente								
				'Ultimi 30 giorni': [moment().subtract(1, 'year').subtract(30, 'days'), moment().subtract(1, 'year')],
				'Ultimi 3 mesi': [moment().subtract(1, 'year').subtract(3, 'months'), moment().subtract(1, 'year').startOf('month')],
				'Ultimi 6 mesi': [moment().subtract(1, 'year').subtract(6, 'months'), moment().subtract(1, 'year').startOf('month')],
				'Ultimo anno': [moment().subtract(1, 'years'), moment().startOf('month')]
			};
							
			$scope.selected_dates = {startDate: $scope.ranges['Ultimi 6 mesi'][0], endDate: $scope.ranges['Ultimi 6 mesi'][1]};
			
			var createChart = function(title, kpi_series)
			{
				$scope.kpichart = new Highcharts.Chart({
				
					chart: {
						type: 'bubble',
						plotBorderWidth: 1,
						renderTo: 'kpichart',
						zoomType: 'xy'						
					},
					
					credits: {
						enabled: false
					},
					
					plotOptions: {
						bubble: {
							tooltip: {
								headerFormat: '<b></b>',
								pointFormat: ' {point.codice}, {series.name} <br>' + 
											 ' <b> Superficie Utile: </b> {point.sup:.2f} mq <br>' +
											 ' <b> (x) Consumo medio al mq rispetto al benchmark: </b> {point.x:.2f}% <br>' +
											 ' <b> (y) Consumo medio al mq rispetto al ' + moment($scope.selected_dates.startDate).add(1,'months').date(0).format("DD-MM-YYYY") + ':</b> {point.y:.2f}% <br>' + 										 
											 ' <b> (r) Potenza di picco: </b> {point.z} MW'
							},
							maxSize: 80,
							minSize: 10
						},
						series: {
							cursor: 'pointer',
							point: {
								events: {
									click: function () { 
											
											$scope.selectedBuilding = BuildingsFactory.getBuilding(this.idImmobile).query(
												function(){
													
													var modalOptions = {											
														headerText: 'Decision Support System (DSS)',
														selectedBuilding: $scope.selectedBuilding
													};
													
													CreemSettings.setBuilding($scope.selectedBuilding);
													
													//TooltipDSS.showModal({}, modalOptions).then(function (result) {});
													TooltipDSS.showModal({}, modalOptions);
												}
											);
									
											

										
									
									}
								}
							}
						}
						
					},

					title: {
						text: ""
					},
			
					xAxis: {
						title: "Consumo medio al mq",
						minPadding: 1,
						maxPadding: 1,					
						max: 200,
						min: -200,
						labels: {
							format: '{value} %'
						},
						gridLineWidth: 1,
						title: {
							text: 'Consumo medio mq (confronto benchmark)'
						},
						plotLines: [{
							color: 'black',
							dashStyle: 'longdash',
							width: 1,
							value: 0,
							label: {
								align: 'middle',
								rotation: 0,
								y: 0,
								style: {
									fontStyle: 'italic'
								},
								text: ''
							},
							zIndex: 3
						}, {
							color: 'white',
							dashStyle: 'dot',
							width: 1,
							value: -170,
							label: {
								align: 'middle',
								rotation: 0,
								y: 20,
								style: {
									fontStyle: 'italic'
								},
								text: 'PRIORITÀ 3  -  Consumo basso / Andamento in peggioramento'
							},
							zIndex: 3
						}, {
							color: 'white',
							dashStyle: 'dot',
							width: 1,
							value: -170,
							label: {
								align: 'middle',
								rotation: 0,
								y: 700,
								style: {
									fontStyle: 'italic'
								},
								text: 'Sostenibilità & Alta Performance'
							},
							zIndex: 3
						}]
					},
					yAxis: {
						minPadding: 1,
						maxPadding: 1,
						max: 200,
						min: -200,
						gridLineWidth: 1,
						labels: {
							format: '{value} %'
						},
						title: {
							text: 'Consumo medio mq (confronto periodo di osservazione)'
						},
						plotLines: [{
							color: 'black',
							dashStyle: 'longdash',
							width: 1,
							value: 0,
							label: {
								align: 'right',
								style: {
									fontStyle: 'italic'
								},
								text: '',
								x: 0
							},
							zIndex: 3
						}, {
							color: 'white',    
							width: 1,
							value: 180,
							label: {
								align: 'right',								
								style: {
									fontStyle: 'italic'
								},
								text: 'PRIORITÀ 1  -  Consumo eccessivo / Andamento in peggioramento',
								x: -30
							},
							zIndex: 3
						}, {
							color: 'white',
							dashStyle: 'dot',
							width: 1,
							value: -180,
							label: {
								align: 'right',
								style: {
									fontStyle: 'italic'
								},
								text: 'PRIORITÀ 2  -  Consumo eccessivo / Andamento in miglioramento',
								x: -20
							},
							zIndex: 3
						}]
					},								

					series: kpi_series
					
					}
				);
			}
			
			

			var getdata = function (startDate, endDate) 
			{
				
				var data = [];
				var kpis = [];
				
				var consumi_medi_mq_data1 = {};
				var consumi_medi_mq_data2 = {};		
				var info_buildings = {};
				var potenza_picco = {};
				var consumi_medi_mq_benchmarks = {};
				var valoremedio_bench = 0;
				var num_benchmarks;
				
				var clusters = {};
				var benchmarks = [];
				var kpi_series = [];
					
				$scope.onClickUpdate = function()
				{	
					getdata( moment($scope.selected_dates.startDate).add(1,'months').date(0).format("YYYY-MM-DD"), moment($scope.selected_dates.endDate).add(1,'months').date(0).format("YYYY-MM-DD"));
				}
				
				var cluster_string; 
				if($scope.selected_cluster == "ALL")
					cluster_string = "LAYOUT;DIREZIONALE;TRADIZIONALE;ITALPOSTE;CMP;IND";
				else
					cluster_string = $scope.selected_cluster;
					
				var superficie_dss;	
				
				if ($scope.selected_superficieDss == "100")
					superficie_dss = "0;100";
				else if ($scope.selected_superficieDss == "300")
					superficie_dss = "100;300";
				else if ($scope.selected_superficieDss == "600")
					superficie_dss = "300;600";
				else if ($scope.selected_superficieDss == "1500")
					superficie_dss = "600;1500";
				else if ($scope.selected_superficieDss == "+1500")
					superficie_dss = "+1500";
				else 
					superficie_dss = "ALL";
										
				var xx;
				var yy;
				
				var consumimedi1 = KPIFactory.getKPI(startDate, "ReqF002", "M", cluster_string, superficie_dss).query( 
					function(){
						var consumimedi2 = KPIFactory.getKPI(endDate, "ReqF002", "M", cluster_string, superficie_dss).query( 
							
							function(){
								
								var potenzapicco = KPIFactory.getKPI(startDate, "ReqF001", "M", cluster_string, superficie_dss).query(
								
									function(){
									
										for(var i=0; i<consumimedi1.length; i++)
										{
											try{
												clusters[ consumimedi1[i].clusterEnergy ].push(consumimedi1[i].idImmobile);
											}
											catch(err){													
												clusters[ consumimedi1[i].clusterEnergy ] = [];
												clusters[ consumimedi1[i].clusterEnergy ].push(consumimedi1[i].idImmobile);
											}																
											
												consumi_medi_mq_data1[ consumimedi1[i].idImmobile ] = consumimedi1[i].valoreKpi;
												consumi_medi_mq_benchmarks[ consumimedi1[i].idImmobile ] = consumimedi2[i].valoreBench;
												info_buildings[consumimedi1[i].idImmobile] = { codice: consumimedi1[i].codice, idImmobile: consumimedi1[i].idImmobile, zona: consumimedi1[i].zona, sup: consumimedi1[i].superficieUtile};
											
										}
																				
										for(var i=0; i<consumimedi2.length; i++)
											consumi_medi_mq_data2[ consumimedi2[i].idImmobile ] = consumimedi2[i].valoreKpi;
											
										for(var i=0; i<potenzapicco.length; i++)
											potenza_picco[ potenzapicco[i].idImmobile ] = potenzapicco[i].valoreKpi; 

																					
										num_benchmarks = Object.keys(consumi_medi_mq_benchmarks).length;

										for(var bench in consumi_medi_mq_benchmarks )
										{
											//console.log(consumi_medi_mq_benchmarks[bench]);
											valoremedio_bench += consumi_medi_mq_benchmarks[bench];											
										}
										
										valoremedio_bench = valoremedio_bench/num_benchmarks;
										
										for(var cluster in clusters )
										{
											kpis = [];
											benchmarks = [];
											
											benchmarks.push( {x:  parseFloat((consumi_medi_mq_benchmarks[ clusters[cluster][0] ]/valoremedio_bench)*100 - 100).toFixed(2), y: 0.0, z: 0.01 , name: clusters[cluster][i] } );																							
											
											
											for(var i=0; i< clusters[cluster].length; i++)
												{		
													if(consumi_medi_mq_data1[ clusters[cluster][i] ]!=null && consumi_medi_mq_data2[ clusters[cluster][i] ]!=null && potenza_picco[ clusters[cluster][i] ]!=null)												
													{	
														xx = (parseFloat((consumi_medi_mq_data2[ clusters[cluster][i] ]/valoremedio_bench)*100 - 100).toFixed(2));
														yy = (parseFloat((parseFloat(consumi_medi_mq_data2[ clusters[cluster][i] ]).toFixed(2)/parseFloat( consumi_medi_mq_data1[ clusters[cluster][i] ]).toFixed(2))*100.0-100.0));
													
														kpis.push( {x: xx , y: yy, z: parseFloat( potenza_picco[ clusters[cluster][i] ]/1000.0 ).toFixed(2), name: (clusters[cluster][i]), codice: info_buildings[clusters[cluster][i]].codice, idImmobile: info_buildings[clusters[cluster][i]].idImmobile, zona: info_buildings[clusters[cluster][i]].zona, sup: info_buildings[clusters[cluster][i]].sup, marker: {fillColor: {radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },stops: [[0, 'rgb(2,2,2)'],[1, get_rgb(xx,yy)]]}}
														});																																																																																		
														
													}
												}																					
												
																								
												kpi_series.push( 
												{
													name: cluster,
													data: kpis,
													
													color: '#69BABD',
													showInLegend: false
												});
										}
										createChart("Valutazione KPI", kpi_series);
									}							
								);
							}					
						);
					}
				);			
			}			
			
			getdata( moment($scope.selected_dates.startDate).add(1, 'months').date(0).format("YYYY-MM-DD"), moment($scope.selected_dates.endDate).add(1,'months').date(0).format("YYYY-MM-DD"));
			
			function get_rgb(valx,valy) {
				var rgb='';
				
				var x = parseInt(valx);
				var y = parseInt(valy);
				if ((x<0 && y<0) || ((y+x)<0)) {
					rgb = '#00FF66';
				}
				else 
					rgb = '#FF0000';
				return rgb;
			};


		}],
		
		templateUrl: 'directives/kpi-dss.html'
  };
  
});
	
	