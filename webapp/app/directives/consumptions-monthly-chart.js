CREEMapp.directive('consumptionsMonthlyChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'MonthlyConsumptionsBandsFactory',
	
		function($scope, CreemSettings, MonthlyConsumptionsBandsFactory)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			
			var createChart = function(title, stitle)
			{
				var monthlyconsumptionschart = new Highcharts.Chart({
				chart: {
						renderTo: 'monthlyconsumptionschart',
						type: 'column'
					},
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					subtitle: {
						text: stitle
					},
					xAxis: {
						categories: [
							'Gen',
							'Feb',
							'Mar',
							'Apr',
							'Mag',
							'Giu',
							'Lug',
							'Ago',
							'Set',
							'Ott',
							'Nov',
							'Dic'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Energia Consumata (kWh)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f} kWh </b> (<i>{point.percentage:.0f} %</i>)</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0,
							stacking: 'value'
						} 
					},
					series: $scope.dataset
				});
			}
							

			var getdata = function (par, type) 
			{
				
					var data = [];
					
					if(type == "POD")
						data = MonthlyConsumptionsBandsFactory.getBuildingConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, CreemSettings.selectedbuildings[0].denominazione + ', ' + CreemSettings.selectedbuildings[0].nome + ' (' + CreemSettings.selectedbuildings[0].siglaprov + ')'); });
					else if(type == "CLUSTER")
						data = MonthlyConsumptionsBandsFactory.getClusterConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, CreemSettings.selectedClusters[0]); });
					else if(type == "PROV") 
						data = MonthlyConsumptionsBandsFactory.getCityConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, CreemSettings.selectedCities[0]); });							
					
			}
						
			var prepareChart = function(data, title)
			{				
				var consumi_F1 = [0,0,0,0,0,0,0,0,0,0,0,0];
				var consumi_F2 = [0,0,0,0,0,0,0,0,0,0,0,0];
				var consumi_F3 = [0,0,0,0,0,0,0,0,0,0,0,0];
								
				var currentTime = new Date();
				for(var i=0; i<data.length; i++)
				{
					//if(data[i].anno == currentTime.getFullYear() - 2 ) 
					{	
						var index = data[i].mese - 1;
						consumi_F1[index]=data[i].F1;
						consumi_F2[index]=data[i].F2;
						consumi_F3[index]=data[i].F3;									
						
					}													
				}
				$scope.dataset = [
				{
					name: 'F3 (Lun-Ven 23-7, Dom e fest: 0:24)',
					data: consumi_F3,
					color: '#009030'					
				},
				{
					name: 'F2 (Lun-Ven 7-8/19-23, Sab: 7-23)',
					data: consumi_F2,
					color: '#FFCC66'
				},
				{	name: 'F1 (Lun-Ven 8-19)',
					data: consumi_F1,
					color: '#FF6666'
				}];		
				
				//createChart(title, 'Consumi mensili anno ' + (currentTime.getFullYear() - 2).toString() + ' (kWh)');											
				createChart(title, 'Consumi mensili anno ' + CreemSettings.selectedYear + ' (kWh)');											
			}
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);		
					
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].pod, "POD");
					else if(CreemSettings.selectedCities.length > 0)
						getdata(CreemSettings.selectedCities[0], "PROV");
					else if(CreemSettings.selectedClusters.length > 0)
						getdata(CreemSettings.selectedClusters[0], "CLUSTER");
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
   

		}],
		
		templateUrl: 'directives/consumptions-monthly-chart.html',
		scope: true
  };
  
});
	
	