CREEMapp.directive('profilesChart', 

function() 
{

	return {	
    controller: ['$scope', 'CreemSettings', 'ProfilesFactory',
	
		function($scope, CreemSettings, ProfilesFactory) 
		{			
			$scope.dataset = [];
			$scope.baseload;
			
			var createChart = function(title, type, dataset)
			{
				var profileschart = new Highcharts.Chart({
				chart: {
						renderTo: 'profileschart_' + type,
						type: 'column'
					},
				
					title: {
						text: title
					},
					xAxis: {
						categories: [
							'01:00',
							'02:00',
							'03:00',
							'04:00',
							'05:00',
							'06:00',
							'07:00',
							'08:00',
							'09:00',
							'10:00',
							'11:00',
							'12:00',
							'13:00',
							'14:00',
							'15:00',
							'16:00',
							'17:00',
							'18:00',
							'19:00',
							'20:00',
							'21:00',
							'22:00',
							'23:00',
							'00:00'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: '(kW)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f} kW</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					
					plotOptions: {
					column: {
							grouping: false,
							shadow: false,
							borderWidth: 0
						}
					},

				
					series: dataset
					
					
				});
			}
							

			var getdata = function (building) 
			{
				
					var profile_monfri = [];
					var profile_saturday = [];
					var profile_holiday = [];
					
					var profile_cluster_monfri = [];
					var profile_cluster_saturday = [];
					var profile_cluster_holiday = [];
					
					var baseloads = [];
					
					
						var baseload = ProfilesFactory.getBuildingBaseload(building.pod).query(

						function(){
								
								for(var i=0; i<24; i++)
									baseloads.push(baseload[0]['baseload']);
								
								$scope.baseload = parseFloat(baseload[0]['baseload']).toFixed(2);
								$scope.dataset[2] = {
											name: "Carico Notturno",
											data: baseloads
								};
									
								profile_monfri = ProfilesFactory.getBuildingProfilesMonFri(building.pod).query( 
									function(){ 
												profile_saturday = ProfilesFactory.getBuildingProfilesSaturday(building.pod).query(
													function(){
														profile_holiday = ProfilesFactory.getBuildingProfilesHoliday(building.pod).query(
														
															function(){
																console.log(CreemSettings.selectedbuildings[0]);
																profile_cluster_monfri = ProfilesFactory.getClusterProfilesMonFri(CreemSettings.selectedbuildings[0].clusterEnergy, CreemSettings.selectedbuildings[0].zona, CreemSettings.selectedbuildings[0].orarioLunVen).query( 
																		function()
																		{ 					
																			var consumi;
																			consumi = preparedata(profile_monfri); 
																			
																			$scope.dataset[1] = {
																					name: CreemSettings.selectedbuildings[0].denominazione,
																					data: consumi
																			};
																																																										
																			
																			consumi = preparedata(profile_cluster_monfri); 
																			
																			$scope.dataset[0] = {
																					name: "Profilo di riferimento",//CreemSettings.selectedbuildings[0].clusterEnergy + " Lun-Ven: " + CreemSettings.selectedbuildings[0].orarioLunVen + " Zona: " + CreemSettings.selectedbuildings[0].zona,
																					type: "area",
																					data: consumi,
																					color: '#FF0000',
																					fillOpacity: 0.1
																			};
																			
																			createChart("Profilo Giornaliero, Periodo: Lun-Ven", "monfri", $scope.dataset);
																		    
																			profile_cluster_saturday = ProfilesFactory.getClusterProfilesSaturday(CreemSettings.selectedbuildings[0].clusterEnergy, CreemSettings.selectedbuildings[0].zona, CreemSettings.selectedbuildings[0].orarioSab).query( 
																				function()
																				{
																						var consumi;
																						consumi = preparedata(profile_saturday); 
																						
																						$scope.dataset[1] = {
																								name: CreemSettings.selectedbuildings[0].denominazione,
																								data: consumi
																						};
																						
																						consumi = preparedata(profile_cluster_saturday); 
																						
																						$scope.dataset[0] = {
																								name: "Profilo di riferimento",//CreemSettings.selectedbuildings[0].clusterEnergy + " Sab: " + CreemSettings.selectedbuildings[0].orarioSab + " Zona: " + CreemSettings.selectedbuildings[0].zona,
																								type: "area",
																								data: consumi,
																								color: '#FF0000',
																								fillOpacity: 0.1
																						};
																						
																						createChart("Profilo Giornaliero, Periodo: Sab e Festivi", "saturday", $scope.dataset);
																		
																						profile_cluster_holiday = ProfilesFactory.getClusterProfilesHoliday(CreemSettings.selectedbuildings[0].clusterEnergy, CreemSettings.selectedbuildings[0].zona).query( 
																							function()
																							{
																								var consumi;
																								consumi = preparedata(profile_holiday); 
																								
																								$scope.dataset[1] = {
																										name: CreemSettings.selectedbuildings[0].denominazione,
																										data: consumi
																								};
																								
																								consumi = preparedata(profile_cluster_holiday); 
																								
																								/*$scope.dataset[0] = {
																										name: "Profilo di riferimento",//CreemSettings.selectedbuildings[0].clusterEnergy + " Zona: " + CreemSettings.selectedbuildings[0].zona,
																										type: "area",
																										data: consumi,
																										color: '#FF0000',
																										fillOpacity: 0.1
																								};
																								createChart("Profilo Giornaliero, Turno: Festivo", "holiday", $scope.dataset);*/
																								
																							}
																						);
																				}																	
																			);
																		});
																			
																		
															});
													});
									});
									
							});
												
			}
			
			var preparedata = function(data)
			{																			
						var consumi_orari = [];
						consumi_orari.push( data[0]['01_am'] );
						consumi_orari.push( data[0]['02_am'] );
						consumi_orari.push( data[0]['03_am'] );
						consumi_orari.push( data[0]['04_am'] );
						consumi_orari.push( data[0]['05_am'] );
						consumi_orari.push( data[0]['06_am'] );
						consumi_orari.push( data[0]['07_am'] );
						consumi_orari.push( data[0]['08_am'] );
						consumi_orari.push( data[0]['09_am'] );
						consumi_orari.push( data[0]['10_am'] );
						consumi_orari.push( data[0]['11_am'] );
						consumi_orari.push( data[0]['12_pm'] );
						consumi_orari.push( data[0]['01_pm'] );
						consumi_orari.push( data[0]['02_pm'] );
						consumi_orari.push( data[0]['03_pm'] );
						consumi_orari.push( data[0]['04_pm'] );
						consumi_orari.push( data[0]['05_pm'] );
						consumi_orari.push( data[0]['06_pm'] );
						consumi_orari.push( data[0]['07_pm'] );
						consumi_orari.push( data[0]['08_pm'] );
						consumi_orari.push( data[0]['09_pm'] );
						consumi_orari.push( data[0]['10_pm'] );
						consumi_orari.push( data[0]['11_pm'] );
						consumi_orari.push( data[0]['12_am'] );
							

						return consumi_orari;																																					
			}

			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);					
					getdata(CreemSettings.selectedbuildings[0]);					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();
			
		}],
		
		templateUrl: 'directives/profiles-chart.html'
  };
  
});
	
	