CREEMapp.directive('todayconsumptionsChart', 

function() 
{
	
	return {    
    controller: ['$scope', 'CreemSettings', 'ConsumptionsFactory','ProfilesFactory',
	
		function($scope, CreemSettings, ConsumptionsFactory, ProfilesFactory) 
		{			
			$scope.dataset = [];
			$scope.baseload;
			
			var actual_hour = moment().format("H");			
			
			var createChart = function(title, dataset)
			{
				var profileschart = new Highcharts.Chart({
				chart: {
						renderTo: 'todayconsumptions_chart',
						type: 'column'
					},
					
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					xAxis: {
						categories: [
							'01:00',
							'02:00',
							'03:00',
							'04:00',
							'05:00',
							'06:00',
							'07:00',
							'08:00',
							'09:00',
							'10:00',
							'11:00',
							'12:00',
							'13:00',
							'14:00',
							'15:00',
							'16:00',
							'17:00',
							'18:00',
							'19:00',
							'20:00',
							'21:00',
							'22:00',
							'23:00',
							'00:00'
						]
					},
					yAxis:[{
						min: 0,
						title: {
							text: 'Potenza (kW)'
						}
					}],
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f} kW</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							grouping: false,
							shadow: false,
							borderWidth: 0
						},
						spline: {
							lineWidth: 1							
						},
						marker: {
							enabled: false
						},
					},
				
					series: dataset
					
					
				});
			}
							

			var getdata = function (building) 
			{
					var consumptions = [];
					var consumption_profile = [];
					var today = moment().subtract(1,'years').format("YYYY-MM-DD");

								consumptions = ConsumptionsFactory.getConsumptions(building.pod, today , today).query(
									function()
									{
										if(consumptions.lenght == 0)
											return;
											
										var consumi;
										consumi = preparedata(consumptions); 
													
													
										for(var i=actual_hour; i<24; i++)																					
													consumi[i] = 0;
													
										$scope.dataset[0] = {
																color: 'rgba(165,170,217,1)',
																name: "Potenza media oraria",
																pointPadding: 0.05,
																
																data: consumi
															};
															
										
										
											
											if(moment().subtract(1,'years').day() >= 1 && moment().subtract(1,'years').day() <= 5)
												consumption_profile = ProfilesFactory.getBuildingProfilesMonFri(building.pod).query( 
													function()
													{
														consumption_profile = preparedata(consumption_profile); 
														$scope.dataset[1] =  {
																	color: 'rgba(200,0,0,0.6)',
																	type: 'spline',
																	marker: {
																		enabled: false
																	},
																	name: "Profilo Standard",
																	
																	data: consumption_profile
														};
											
													createChart("Profilo giornaliero (" + moment().subtract(1,'years').format("dddd, MMMM Do YYYY") + ")", $scope.dataset);
													}
												);
											else if(moment().subtract(1,'years').day() == 6)
												consumption_profile = ProfilesFactory.getBuildingProfilesSaturday(building.pod).query( 
													function()
													{
														consumption_profile = preparedata(consumption_profile); 
														$scope.dataset[1] =  {
																	color: 'rgba(200,0,0,0.6)',
																	type: 'spline',
																	marker: {
																		enabled: false
																	},
																	name: "Profilo Standard",
																	
																	data: consumption_profile
														};
											
													createChart("Profilo giornaliero (" + moment().subtract(1,'years').format("dddd, MMMM Do YYYY") + ")", $scope.dataset);
													}
												);
											else if( moment().subtract(1,'years').day() == 0)
												consumption_profile = ProfilesFactory.getBuildingProfilesHoliday(building.pod).query( 
													function()
													{
														consumption_profile = preparedata(consumption_profile); 
														$scope.dataset[1] =  {
																	color: 'rgba(200,0,0,0.6)',
																	type: 'spline',
																	marker: {
																		enabled: false
																	},
																	name: "Profilo Standard",
																	
																	data: consumption_profile
														};
											
													createChart("Profilo giornaliero (" + moment().subtract(1,'years').format("dddd, MMMM Do YYYY") + ")", $scope.dataset);
													}
												);
											
												
									}
							
								);
			}
								

			
			var preparedata = function(data)
			{
						var consumi_orari = [];
						if(data.length>0)
						{
							consumi_orari.push( data[0]['01_am'] );
							consumi_orari.push( data[0]['02_am'] );
							consumi_orari.push( data[0]['03_am'] );
							consumi_orari.push( data[0]['04_am'] );
							consumi_orari.push( data[0]['05_am'] );
							consumi_orari.push( data[0]['06_am'] );
							consumi_orari.push( data[0]['07_am'] );
							consumi_orari.push( data[0]['08_am'] );
							consumi_orari.push( data[0]['09_am'] );
							consumi_orari.push( data[0]['10_am'] );
							consumi_orari.push( data[0]['11_am'] );
							consumi_orari.push( data[0]['12_pm'] );
							consumi_orari.push( data[0]['01_pm'] );
							consumi_orari.push( data[0]['02_pm'] );
							consumi_orari.push( data[0]['03_pm'] );
							consumi_orari.push( data[0]['04_pm'] );
							consumi_orari.push( data[0]['05_pm'] );
							consumi_orari.push( data[0]['06_pm'] );
							consumi_orari.push( data[0]['07_pm'] );
							consumi_orari.push( data[0]['08_pm'] );
							consumi_orari.push( data[0]['09_pm'] );
							consumi_orari.push( data[0]['10_pm'] );
							consumi_orari.push( data[0]['11_pm'] );
							consumi_orari.push( data[0]['12_am'] );
						}
							

						return consumi_orari;																																					
			}

			
			var refreshData = function()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);					
					getdata(CreemSettings.selectedbuildings[0]);										
			}
			
			$scope.$on('settingsUpdated', function (event, data){ refreshData(); });
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();
   
			
		}],
		
		templateUrl: 'directives/todayconsumptions-chart.html'
  };
  
});
	
	