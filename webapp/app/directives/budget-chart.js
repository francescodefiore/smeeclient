CREEMapp.directive('budgetChart', 

function() 
{

	return {    
    controller: ['$scope', 'CreemSettings', 'BudgetFactory', 'MonthlyConsumptionsFactory',
	
		function($scope, CreemSettings, BudgetFactory, MonthlyConsumptionsFactory) 
		{			
							
			$scope.dataset = {budget: [], consumptions: [], consumptions_prediction: [],budget_sum: [],consumptions_sum: []};
			$scope.BUDGET = [];
			$scope.CONSUMPTIONS = [];
			$scope.BUDGET_SUM = [];
			$scope.CONSUMPTIONS_SUM = [];
			$scope.CONSUMPTIONS_PREDICTION = [];
			$scope.result = 0;
			
			var createChart = function(title)
			{
				
				var budgetchart = new Highcharts.Chart({
				chart: {
						renderTo: 'budgetchart',
						zoomType: 'xy'
						},
						
						credits: {
							enabled: false
						},
						title: {
							text: title
						},
						xAxis: [{
							categories: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu','Lug', 
										'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
							crosshair: true
						}],
						yAxis: [{ // Primary yAxis
							labels: {
								format: '{value} MW',
								style: {
									color: Highcharts.getOptions().colors[1]
								}
							},
							title: {
								text: 'Consumi/Budget mensili',
								style: {
									color: Highcharts.getOptions().colors[1]
								}
							}
						}, { // Secondary yAxis
							title: {
								text: 'Consumi/Budget totali',
								style: {
									color: Highcharts.getOptions().colors[0]
								}
							},
							labels: {
								format: '{value:.2f} MW',
								style: {
									color: Highcharts.getOptions().colors[0]
								}
							},
							opposite: true
						}],
						
						tooltip: {
							shared: true,
							formatter: function () {
								var points = this.points;
								var pointsLength = points.length;
								var tooltipMarkup = pointsLength ? '<span style="font-size: 10px">' + points[0].key + '</span><br/>' : '';
								var index;
								var y_value;
								var percentuale;
		
								var cons_tot;

								for(index = 0; index < pointsLength; index += 1) {
									y_value = (points[index].y).toFixed(2);

	
								  if (points[index].series.name == "Consumi totali")
									cons_tot = points[index].y;								  

								  if (points[index].series.name == "Budget totali") {
								    
									percentuale = (((parseFloat(cons_tot).toFixed(2) - parseFloat(points[index].y).toFixed(2))/ parseFloat((points[index].y)).toFixed(2))*100).toFixed(2);									
									tooltipMarkup += '<span style="color:' + points[index].series.color + '">\u25CF</span> ' + points[index].series.name + ': <b>' + y_value  + ' MW </b><br/>';
									tooltipMarkup += ' Consumi totali  ' +percentuale+' % rispetto al budget </b><br/>';
								  }
								  else{
										tooltipMarkup += '<span style="color:' + points[index].series.color + '">\u25CF</span> ' + points[index].series.name + ': <b>' + y_value  + ' MW</b><br/>';
								  }
								}

								return tooltipMarkup;
							}
						},
						series: [
						{
								name: 'Consumi mensili',
								type: 'column',
								yAxis: 1,
								data: $scope.dataset.consumptions

						},
						{
								name: 'Budget mensili',
								type: 'column',
								yAxis: 1,
								data: $scope.dataset.budget
								
							},
							
							{
								name: 'Consumi totali',
								type: 'spline',
								data: $scope.dataset.consumptions_sum
							},
							{
								name: 'Budget totali',
								type: 'spline',
								data: $scope.dataset.budget_sum
							}]
						});
			}
							

			var getdata = function (par, type) 
			{
				
					var consumptions = [];
					var budget = [];
					
					if(type == "POD")
						consumptions = MonthlyConsumptionsFactory.getBuildingConsumptionsYear(par,CreemSettings.selectedYear).query( 
							function(){ 
									prepareConsumptions(consumptions); 
									
									budget = BudgetFactory.getBuildingBudgetYear(par,CreemSettings.selectedYear).query(
										function(){ prepareBudget(budget, CreemSettings.selectedbuildings[0].codice + ' ' + CreemSettings.selectedbuildings[0].clusterEnergy + ', ' + CreemSettings.selectedbuildings[0].nome + ' (' + CreemSettings.selectedbuildings[0].siglaprov + ')'); }
									);
									
							});	
					else if(type == "CLUSTER")
						consumptions = MonthlyConsumptionsFactory.getClusterConsumptionsYear(par,CreemSettings.selectedYear).query( 
							function(){ 
									prepareConsumptions(consumptions); 
									
									budget = BudgetFactory.getClusterBudgetYear(par,CreemSettings.selectedYear).query(
										function(){  prepareBudget(budget, CreemSettings.selectedClusters[0]); }
									);
									
							});
					else if(type == "PROV") 
						consumptions = MonthlyConsumptionsFactory.getCityConsumptionsYear(par,CreemSettings.selectedYear).query( 
							function(){ 
									prepareConsumptions(consumptions); 
									
									budget = BudgetFactory.getCityBudgetYear(par,CreemSettings.selectedYear).query(
										function(){ prepareBudget(budget, CreemSettings.selectedCities[0]); }
									);
							
							});							
				
			}
			
			var prepareConsumptions = function(data)
			{			
						var currentTime = new Date();
						var current_month = currentTime.getMonth() + 1;
						var current_year = currentTime.getFullYear();
		
						for(var i=0; i<data.length; i++)
						{
							if(data[i].anno == CreemSettings.selectedYear)		
							{								
								$scope.CONSUMPTIONS[0] = data[i].gen/1000;
								$scope.CONSUMPTIONS[1] = data[i].feb/1000;
								$scope.CONSUMPTIONS[2] = data[i].mar/1000;
								$scope.CONSUMPTIONS[3] = data[i].apr/1000;
								$scope.CONSUMPTIONS[4] = data[i].mag/1000;
								$scope.CONSUMPTIONS[5] = data[i].giu/1000;
								$scope.CONSUMPTIONS[6] = data[i].lug/1000;
								$scope.CONSUMPTIONS[7] = data[i].ago/1000;
								$scope.CONSUMPTIONS[8] = data[i].sett/1000;
								$scope.CONSUMPTIONS[9] = data[i].ott/1000;
								$scope.CONSUMPTIONS[10] = data[i].nov/1000;
								$scope.CONSUMPTIONS[11] = data[i].dic/1000;					
							}
						}
						
						if(CreemSettings.selectedYear == current_year)
						{
							for(var i=0; i<12; i++)
							{
								if(current_month < i + 2)		
								{								
									$scope.CONSUMPTIONS_PREDICTION[i] = $scope.CONSUMPTIONS[i];
									$scope.CONSUMPTIONS[i] = 0;								
								}
								else
									$scope.CONSUMPTIONS_PREDICTION[i] = 0;
							}
						}
						
						$scope.dataset.consumptions = $scope.CONSUMPTIONS;
						$scope.dataset.consumptions_prediction = $scope.CONSUMPTIONS_PREDICTION;
						$scope.CONSUMPTIONS_SUM[0] = $scope.CONSUMPTIONS[0] + $scope.CONSUMPTIONS_PREDICTION[0];
						for(var i=1; i<12; i++)
						{
							if(CreemSettings.selectedYear == current_year)
								$scope.CONSUMPTIONS_SUM[i] = $scope.CONSUMPTIONS_SUM[i-1] + $scope.CONSUMPTIONS[i] + $scope.CONSUMPTIONS_PREDICTION[i];
							else
								$scope.CONSUMPTIONS_SUM[i] = $scope.CONSUMPTIONS_SUM[i-1] + $scope.CONSUMPTIONS[i];
						}
							
						$scope.dataset.consumptions_sum = $scope.CONSUMPTIONS_SUM;
																
			}
			
			
			var prepareBudget = function(data, title)
			{			
						var currentTime = new Date();
						var current_year = currentTime.getFullYear();
		
		
						for(var i=0; i<data.length; i++)
						{
							if(data[i].anno == CreemSettings.selectedYear)		
							{								
								$scope.BUDGET[0] = data[i].gen/1000;
								$scope.BUDGET[1] = data[i].feb/1000;
								$scope.BUDGET[2] = data[i].mar/1000;
								$scope.BUDGET[3] = data[i].apr/1000;
								$scope.BUDGET[4] = data[i].mag/1000;
								$scope.BUDGET[5] = data[i].giu/1000;
								$scope.BUDGET[6] = data[i].lug/1000;
								$scope.BUDGET[7] = data[i].ago/1000;
								$scope.BUDGET[8] = data[i].sett/1000;
								$scope.BUDGET[9] = data[i].ott/1000;
								$scope.BUDGET[10] = data[i].nov/1000;
								$scope.BUDGET[11] = data[i].dic/1000;	
							}
						}
						
						$scope.dataset.budget = $scope.BUDGET;
						$scope.BUDGET_SUM[0] = $scope.BUDGET[0];
						for(var i=1; i<12; i++)						
							$scope.BUDGET_SUM[i] = $scope.BUDGET_SUM[i-1] + $scope.BUDGET[i];
							
						$scope.dataset.budget_sum = $scope.BUDGET_SUM;
						
						
						var meseCorrente = 11;
						if(CreemSettings.selectedYear == current_year)		
							meseCorrente = currentTime.getMonth()-1;
							
						$scope.stimaBudget = - parseFloat(($scope.BUDGET_SUM[11] - $scope.CONSUMPTIONS_SUM[11])/$scope.BUDGET_SUM[11] *100).toFixed(2);
						var consumiMese = - parseFloat(($scope.BUDGET_SUM[11] - $scope.CONSUMPTIONS_SUM[meseCorrente])/$scope.BUDGET_SUM[11] *100).toFixed(2);

						var chart = $('#gaugeBudget').highcharts(),
							point,
							newVal; 
							
						newVal = 100 + consumiMese;
						chart.series[0].points[0].update(Math.round(newVal*100)/100);
						
						createChart(title);											
			}


			var refreshData = function ()
			{										
					$scope.dataset = {budget: [], consumptions: [], budget_sum: [], consumptions_sum: []};
					$scope.BUDGET = [];
					$scope.CONSUMPTIONS = [];
					$scope.BUDGET_SUM = [];
					$scope.CONSUMPTIONS_SUM = [];		
					
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].pod, "POD");
					else if(CreemSettings.selectedCities.length > 0)
						getdata(CreemSettings.selectedCities[0], "PROV");
					else if(CreemSettings.selectedClusters.length > 0)
						getdata(CreemSettings.selectedClusters[0], "CLUSTER");
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
   
			

		}],
		
		templateUrl: 'directives/budget-chart.html'
  };
  
});
	
	