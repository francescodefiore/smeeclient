CREEMapp.directive('settingsSingleSelectionAndYear', function() {

	  return {		
		templateUrl: 'directives/settings-single-selection-and-year.html',
				
		controller: ['$scope', 'CreemSettings', function($scope, CreemSettings) {
							
							$scope.selected_cluster = "LAYOUT";
							
							$scope.dates = {
								date8: new Date()
							};
							$scope.dates.date8.setFullYear(2015);
							
							CreemSettings.setYear($scope.dates.date8.getFullYear());
							
							$scope.open = {
							date8: false
							};
							
							$scope.setCluster = function() 
							{													
								CreemSettings.setCluster($scope.selected_cluster);
							}
							
							$scope.selected_city = "AG";
							$scope.setCity = function() 
							{													
								CreemSettings.setCity($scope.selected_city);
							}
							
							$scope.setBuilding = function() 
							{													
								CreemSettings.setBuilding($scope.selected_building);
							}
							
							$scope.setYear = function(year)
							{
								CreemSettings.setYear($scope.dates.date8.getFullYear());
							}
							
							$scope.onClickUpdate = function (tab)
							{
								$scope.setYear($scope.dates.date8.getFullYear());
								
								if($scope.currentTab == 'building_tab')
									$scope.setBuilding();								
								if($scope.currentTab == 'cluster_tab')
									$scope.setCluster();									
								if($scope.currentTab == 'city_tab')
									$scope.setCity();
							}
							
							
							$scope.building_tab = {	name: 'building_tab'};					
							$scope.cluster_tab = {	name: 'cluster_tab'};
							$scope.city_tab = {	name: 'city_tab'};
							

								$scope.currentTab = 'building_tab';

								$scope.onClickTab = function (tab) {
									$scope.currentTab = tab.name;
									
								}
								
								$scope.isActiveTab = function(name) {
									return name == $scope.currentTab;
								}


							// Disable today selection
							$scope.disabled = function(date, mode) {
								return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
							};

							$scope.dateOptions = {
								showWeeks: false,
								startingDay: 1
							};

							$scope.timeOptions = {
								readonlyInput: false,
								showMeridian: false
							};

							$scope.dateModeOptions = {
								minMode: 'year',
								maxMode: 'year'
							};

							$scope.openCalendar = function(e, date) {
								$scope.open[date] = true;
							};
							
							 var unwatch = $scope.$watch(function() {
								return $scope.dates;
							}, function() {
									this.dayRange = 'n/a';
								
							}, true);

							$scope.$on('$destroy', function() {
								unwatch();
			});
														
		}]}
	});