CREEMapp.directive('buildingSelection', function() {

	  return {		
		
		templateUrl: 'directives/building-selection.html',
				
		controller: ['$scope', 'BuildingsFactory', 'CreemSettings','$cookies', function($scope, BuildingsFactory, CreemSettings,$cookies) {
							
							$scope.selected_building;
																										
							$scope.immobili = BuildingsFactory.getBuildings().query(
								function(){
									if(CreemSettings.selectedbuildings.length == 0)
									{	
										var cBuilding = $cookies.getObject('building');
										if(cBuilding != null)
										{
											CreemSettings.setBuilding(cBuilding);
											$cookies.remove('building');
										}
										else
										{
											//facciamo partire con PAX1400
											//CreemSettings.setBuilding($scope.immobili[0]);
											for(var i=0;i<$scope.immobili.length;i++)
											{
												if($scope.immobili[i]['idImmobile'] == 10)
												{
													CreemSettings.setBuilding($scope.immobili[i]);
													break;
												}
											}
										}
									}	
									$scope.selected_building = CreemSettings.selectedbuildings[0];	
								}
							);

							$scope.setBuilding = function() 
							{													
								CreemSettings.setBuilding($scope.selected_building);
							}
												
						}
		],
		
		link: function(scope, element)
			  { 		
					
					$(element).find('#buildings_list').val(scope.selected_building);										
					scope.$watch('immobili', function(newValue, oldValue) {																	
										if (newValue.length != 0)																				
										{
											$(element).find('#buildings_list').chosen();
										}
										
								}, true);	
								
				}
				
		
		
	  }
	});
	
	