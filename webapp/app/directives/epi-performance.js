CREEMapp.directive('epiPerformance', 

function() 
{
	
	return {    
	scope: {},
    controller: ['$scope', 'CreemSettings', 'EPIFactory',
	
		function($scope, CreemSettings, EPIFactory) 
		{			
							
			$scope.epi = {EPI_SL: 0, EPI_SL_GG: 0, EPI_VL: 0, EPE_SL: 0, EPE_SL_GGe: 0, EPE_VL: 0};								
			$scope.bench = {EPI_SL_Bench: 0, EPI_SL_GG_Bench: 0, EPI_VL_Bench: 0, EPE_SL_Bench: 0, EPE_SL_GGe_Bench: 0, EPE_VL_Bench: 0};				
			$scope.tollerance = {EPI_SL: 20, EPI_SL_GG: 20, EPI_VL: 20, EPE_SL: 20, EPE_SL_GGe: 20, EPE_VL: 20};
					
					
			var createGauge = function(title, unit, divchart, epi, bench, tollerance)
			{
								
				var budgetchart = new Highcharts.Chart({
				
						chart: {
							type: 'gauge',
							renderTo: divchart,
							plotBackgroundColor: null,
							plotBackgroundImage: null,
							plotBorderWidth: 0,
							plotShadow: false
						},
						
						credits: {
							enabled: false
						},

						title: {
							text: title,
							style:{								
								fontSize: '15px'
							}
						},
						
						subtitle: {
							text: unit
						},

						pane: {
							startAngle: -150,
							endAngle: 150,
							background: [{
								backgroundColor: {
									linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
									stops: [
										[0, '#FFF'],
										[1, '#333']
									]
								},
								borderWidth: 0,
								outerRadius: '109%'
							}, {
								backgroundColor: {
									linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
									stops: [
										[0, '#333'],
										[1, '#FFF']
									]
								},
								borderWidth: 1,
								outerRadius: '107%'
							}, {
								// default background
							}, {
								backgroundColor: '#DDD',
								borderWidth: 0,
								outerRadius: '105%',
								innerRadius: '103%'
							}]
						},
						

						// the value axis
						yAxis: {
							min: 0,
							max: 4*bench,

							minorTickInterval: 'auto',
							minorTickWidth: 1,
							minorTickLength: 10,
							minorTickPosition: 'inside',
							minorTickColor: '#666',

							tickPixelInterval: 30,
							tickWidth: 2,
							tickPosition: 'inside',
							tickLength: 10,
							tickColor: '#666',
							labels: {
								step: 2,
								rotation: 'auto'
							},							
							plotBands: [{
								from: 0,
								to: bench,
								color: '#55BF3B' // green
							}, {
								from: bench,
								to: 2*(bench*(1+tollerance/100.0)),
								color: '#DDDF0D' // yellow
							}, {
								from: 2*(bench*(1+tollerance/100.0)),
								to: 4*bench,
								color: '#DF5353' // red
							}]
						},				

						series: [{
							name:  title,
							data: [epi]							
						}]
				
    
				
				
						});
			}					


			var refreshData = function ()
			{		
					var building = CreemSettings.selectedbuildings[0];
					
															
					$scope.diff = {};					
								
					var data = EPIFactory.getEPI(building.idImmobile, 2014).query(
						function()
						{
							var kpi;
							if(data.length != 0)
							{
								kpi = data[0];
								
								$scope.epi["EPI_SL"] = kpi["EPI_SL"];
								$scope.bench["EPI_SL_Bench"] = kpi["EPI_SL_Bench"];								
								$scope.diff["EPI_SL"] = parseFloat(($scope.epi["EPI_SL"]/$scope.bench["EPI_SL_Bench"] - 1)*100).toFixed(2);
								
								$scope.epi["EPI_SL_GG"] = kpi["EPI_SL_GG"];
								$scope.bench["EPI_SL_GG_Bench"] = kpi["EPI_SL_GG_Bench"];								
								$scope.diff["EPI_SL_GG"] = parseFloat(($scope.epi["EPI_SL_GG"]/$scope.bench["EPI_SL_GG_Bench"] - 1)*100).toFixed(2);
								
								$scope.epi["EPI_VL"] = kpi["EPI_VL"];
								$scope.bench["EPI_VL_Bench"] = kpi["EPI_VL_Bench"];								
								$scope.diff["EPI_VL"] = parseFloat(($scope.epi["EPI_VL"]/$scope.bench["EPI_VL_Bench"] - 1)*100).toFixed(2);
								
								$scope.epi["EPE_SL"] = kpi["EPE_SL"];
								$scope.bench["EPE_SL_Bench"] = kpi["EPE_SL_Bench"];								
								$scope.diff["EPE_SL"] = parseFloat(($scope.epi["EPE_SL"]/$scope.bench["EPE_SL_Bench"] - 1)*100).toFixed(2);
								
								$scope.epi["EPE_SL_GGe"] = kpi["EPE_SL_GGe"];
								$scope.bench["EPE_SL_GGe_Bench"] = kpi["EPE_SL_GGe_Bench"];								
								$scope.diff["EPE_SL_GGe"] = parseFloat(($scope.epi["EPE_SL_GGe"]/$scope.bench["EPE_SL_GGe_Bench"] - 1)*100).toFixed(2);
								
								$scope.epi["EPE_VL"] = kpi["EPE_VL"];
								$scope.bench["EPE_VL_Bench"] = kpi["EPE_VL_Bench"];								
								$scope.diff["EPE_VL"] = parseFloat(($scope.epi["EPE_VL"]/$scope.bench["EPE_VL_Bench"] - 1)*100).toFixed(2);
							}
							
							createGauge("EPi Superficie lorda", "[kWh/mq*anno]", "gauge_episl", $scope.epi["EPI_SL"], $scope.bench["EPI_SL_Bench"], $scope.tollerance["EPI_SL"]);
							createGauge("EPi Superficie lorda (GG)", "[W/mqK]", "gauge_episl_gg", $scope.epi["EPI_SL_GG"], $scope.bench["EPI_SL_GG_Bench"], $scope.tollerance["EPI_SL_GG"]);
							createGauge("EPi Volume lordo", "[kWh/m3*anno]", "gauge_epivl", $scope.epi["EPI_VL"], $scope.bench["EPI_VL_Bench"], $scope.tollerance["EPI_VL"]);

							createGauge("EPe Superficie lorda", "[kWh/mq*anno]", "gauge_epesl", $scope.epi["EPE_SL"], $scope.bench["EPE_SL_Bench"], $scope.tollerance["EPE_SL"]);
							createGauge("EPe Superficie lorda (GG)", "[W/mqK]", "gauge_epesl_gge", $scope.epi["EPE_SL_GGe"], $scope.bench["EPE_SL_GGe_Bench"], $scope.tollerance["EPE_SL_GGe"]);
							createGauge("EPe Volume lordo", "[kWh/mc*anno]", "gauge_epevl", $scope.epi["EPE_VL"], $scope.bench["EPE_VL_Bench"], $scope.tollerance["EPE_VL"]);								
							
							
						}
					
					);

			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
	
			
			
		}],
		
		templateUrl: 'directives/epi-performance.html'
  };
  
});
	
	