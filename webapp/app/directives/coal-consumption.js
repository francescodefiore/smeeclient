CREEMapp.directive('coalConsumption',

function()
{

	return {
    controller: ['$scope', 'CreemSettings', '$http', 'CoalFactory',

		function($scope, CreemSettings, $http, CoalFactory)
		{
			var refreshData = function()
			{
				$scope.pod = CreemSettings.selectedbuildings[0].pod;

				var data = CoalFactory.getConsumptions($scope.pod).query(
                    function() {
                        $scope.coalData = data;
                    }
                );
			}

			$scope.$on('settingsUpdated', function (event, data) { refreshData(); });

			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();

		}],

		templateUrl: 'directives/coal-consumption.html'
  };

});

