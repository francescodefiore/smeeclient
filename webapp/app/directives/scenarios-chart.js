CREEMapp.directive('scenariosChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'ManutenzioneFactory', 'TooltipScenarioService',
	
		function($scope, CreemSettings, ManutenzioneFactory, TooltipScenarioService)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			$scope.scenari = [];
			var scenarioschart;
			
			var createChart = function(title, stitle)
			{
					scenarioschart = new Highcharts.Chart({				
										
					chart: {
						renderTo: 'scenarioschart',
						type: 'scatter',
						zoomType: 'xy'
					},
					title: {
						text: title
					},
					subtitle: {
						text: stitle
					},
					xAxis: {
						title: {
							enabled: true,
							text: 'Costo Totale (Euro)'
						},
						startOnTick: true,
						endOnTick: true,
						showLastLabel: true
					},
					yAxis: {
						title: {
							text: 'Risparmio EPI (kWh*mq/anno)'
						}
					},					
					plotOptions: {
						scatter: {							
							marker: {
								radius: 10,
								states: {
									hover: {
										enabled: true,
										lineColor: 'rgb(100,100,100)'
									}
								}
							},
							states: {
								hover: {
									marker: {
										enabled: false
									}
								}
							},
							tooltip: {								
								headerFormat: '',
								pointFormat: '<b>Scenario</b> {point.id}<br>'+
								'<b>Costo Totale:</b> {point.x} Euro <br><b>Risparmio EPI:</b> {point.y} kWh*mq/anno'								
							}
						},
						series: {
							cursor: 'pointer',
							point: {
								events: {
									click: function () {
										var interventi1 = [];
										var interventi2 = [];
										var interventi3 = [];
										var interventi4 = [];
										
										for(var i=0;i<$scope.scenari[this.id].interventi.length; i++)
										{
											if($scope.scenari[this.id].interventi[i].tipologiaIntervento == 1 || $scope.scenari[this.id].interventi[i].tipologiaIntervento == 2 || $scope.scenari[this.id].interventi[i].tipologiaIntervento == 9)
												interventi1.push($scope.scenari[this.id].interventi[i]);
											else if($scope.scenari[this.id].interventi[i].tipologiaIntervento == 3)
												interventi2.push($scope.scenari[this.id].interventi[i]);
											else if($scope.scenari[this.id].interventi[i].tipologiaIntervento == 4 || $scope.scenari[this.id].interventi[i].tipologiaIntervento == 5)
												interventi3.push($scope.scenari[this.id].interventi[i]);
											else if($scope.scenari[this.id].interventi[i].tipologiaIntervento == 6)
												interventi4.push($scope.scenari[this.id].interventi[i]);
												
										}
										
										var modalOptions = {											
											headerText: 'Scenario Manutenzione Straordinaria N°' + this.id,
											costoTotale: $scope.scenari[this.id].costoTotale,
											ritornoInvestimento: Math.ceil(12*($scope.scenari[this.id].costoTotale/($scope.scenari[this.id].epiAttuale*CreemSettings.selectedbuildings[0].superficieUtile*0.164))),
											epiAttuale: $scope.scenari[this.id].epiAttuale,
											epiRisparmio: $scope.scenari[this.id].epiRisparmio,
											co2Attuale: $scope.scenari[this.id].epiAttuale*CreemSettings.selectedbuildings[0].superficieUtile*0.41/1000,
											co2Simulato: $scope.scenari[this.id].epiRisparmio*CreemSettings.selectedbuildings[0].superficieUtile*0.41/1000,
											interventiOpaco: interventi1,
											interventiTrasp: interventi2,
											interfventiIll: interventi4,
											interventiClima: interventi3
											
										};

										//TooltipScenarioService.showModal({}, modalOptions).then(function (result) {});
										TooltipScenarioService.showModal({}, modalOptions);
									}
								}
							}
						}
					},
					series: [{
						name: 'Scenari Manutentivi',
						color: 'rgba(223, 83, 83, .5)',						
						data: [ {id: 0, x: $scope.scenari[0].costoTotale, y: $scope.scenari[0].epiRisparmio}, 
								{id: 1, x: $scope.scenari[1].costoTotale, y: $scope.scenari[1].epiRisparmio},
								{id: 2, x: $scope.scenari[2].costoTotale, y: $scope.scenari[2].epiRisparmio},
								{id: 3, x: $scope.scenari[3].costoTotale, y: $scope.scenari[3].epiRisparmio},
								{id: 4, x: $scope.scenari[4].costoTotale, y: $scope.scenari[4].epiRisparmio},
								{id: 5, x: $scope.scenari[5].costoTotale, y: $scope.scenari[5].epiRisparmio},
								{id: 6, x: $scope.scenari[6].costoTotale, y: $scope.scenari[6].epiRisparmio},
								{id: 7, x: $scope.scenari[7].costoTotale, y: $scope.scenari[7].epiRisparmio},
								{id: 8, x: $scope.scenari[8].costoTotale, y: $scope.scenari[8].epiRisparmio},
								{id: 9, x: $scope.scenari[9].costoTotale, y: $scope.scenari[9].epiRisparmio},

							  ]

					}]
					
				});
			}
							

			var getdata = function (par) 
			{
				
					var data = [];
															
					data = ManutenzioneFactory.getScenariManutentivi(par).query( 
						function()
						{
							prepareChart(data, "Scenari Manutenzione Straordinaria (Best Value for Money)"); 
						},
						function(error)
						{
							createChart("Scenari Manutenzione Straordinaria (Best Value for Money)","");
							while(scenarioschart.series.length > 0)
								scenarioschart.series[0].remove(true);
						}
					);					
					
			}
						
			var prepareChart = function(data, title)			
			{		
				$scope.scenari = data;			
				createChart(title, "Immobile " + CreemSettings.selectedbuildings[0].codice);											
			}
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);		
					
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].idImmobile);					
					
			}
			
			
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
   
			$scope.dettagliScenario = function(idScenario){
				
			};
		}],
		
		templateUrl: 'directives/scenarios-chart.html',
		scope: true
  };
  
});
	
	