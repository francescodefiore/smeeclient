CREEMapp.directive('settingsBuildingAcotelSelection', function() {
	
	  return {
		scope: {},
	  
		templateUrl: 'directives/settings-building-acotel-selection.html',
				
		controller: ['$scope', 'BuildingsFactory', 'CreemSettings',   function($scope, BuildingsFactory, CreemSettings) {
									
									
							$scope.selectedBuildings = [];
							
							$scope.selected_buildings = [];

							$scope.selected_weather = '';
							
							$scope.immobili = BuildingsFactory.getBuildingsAcotel().query(
								function(){
									
									if(CreemSettings.selectedbuildingsAcotel.length == 0)
									{
										CreemSettings.setBuildingAcotel($scope.immobili[0]);
									}
									
									for(var i=0; i<CreemSettings.selectedbuildingsAcotel.length; i++)
									{									
										$scope.selected_buildings.push( CreemSettings.selectedbuildingsAcotel[i].codice);
										$scope.selectedBuildings = CreemSettings.selectedbuildingsAcotel;
									}
									
									
								}
							);
							

							$scope.selectedDate = CreemSettings.selectedDatesAcotel; 
							$scope.selected_clusters = CreemSettings.selectedClusters;
							
							$scope.selected_dates = {startDate: moment(CreemSettings.selectedDatesAcotel.from), endDate: moment(CreemSettings.selectedDatesAcotel.to)}
							
							$scope.ranges = {							
								// CREEM: L'anno corrente è il precedente								
								'Ieri': [moment().subtract(1, 'days').subtract(1,'years'), moment().subtract(1, 'days').subtract(1,'years')],
								'Ultimi 7 giorni': [moment().subtract(7, 'days').subtract(1,'years'), moment().subtract(1,'years')],
								'Ultimi 30 giorni': [moment().subtract(30, 'days').subtract(1,'years'), moment().subtract(1,'years')],
								'Mese corrente': [moment().startOf('month').subtract(1,'years'), moment().subtract(1, 'days').subtract(1,'years')]
							};
														
							
							$scope.setFilters = function() {
																								
								var selDates = {from:  moment($scope.selected_dates.startDate).format("YYYY-MM-DD"), to: moment($scope.selected_dates.endDate).format("YYYY-MM-DD")};															
								var conf = { buildings: $scope.selectedBuildings,buildingsAcotel: $scope.selectedBuildings, clusters: $scope.selected_clusters, dates: selDates};
								CreemSettings.setSettingsAcotel(conf);
							}
														
						}
		],
		
		link: function(scope, element)
			  { 			  			
					$(element).find('#clusters_list').val(scope.selected_clusters);	
					$(element).find('#date_range').val(scope.selectedDate.from + " - " + scope.selectedDate.to);
					$(element).find('#buildings_list').val(scope.selected_buildings);
					
					scope.$watch('immobili', function(newValue, oldValue) {																	
																				
										if (newValue.length != 0)																				
											$(element).find('.chosen-select').chosen();
										
								}, true);
					
				}
		
		
	  }
	});
	
	