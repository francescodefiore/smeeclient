CREEMapp.directive('manutenzioneStraordinariaTable', 
function() 
{
	return {    
			templateUrl: 'directives/manutenzione-straordinaria-table.html',
			
			controller: ['$scope', 'ManutenzioneFactory', 'ngTableParams', 'LoadingFactory', 'CreemSettings', 
			
			function($scope, ManutenzioneFactory, ngTableParams, LoadingFactory, CreemSettings) 
			{		
				var filter_immobile = "";
				var filter_cluster = "";
				var filter_prov = "";
				
				if(CreemSettings.selectedbuildings.length > 0)				
						filter_immobile = CreemSettings.selectedbuildings[0].codice;

				
				var interventi = [{"id":"Sostituzione", "title": "Sostituzione" }];
				var filter_codice = "";
				var filter_cluster = "";
				var filter_prov = "";
				var filter_tipologia = "";
				
				if(CreemSettings.selectedbuildings.length > 0)					
						filter_codice = CreemSettings.selectedbuildings[0].codice;
						
				
				// var m_data =  ManutenzioneFactory.getTipoIntervento().query(
						// function()
						// {
							// if (m_data.length > 0) {
								// interventi.push(  {"id": "" , "title": "Tutti" } );
								// for(var i=0; i<m_data.length; i++) {
									// interventi.push({"id":m_data[i].descrizione, "title": m_data[i].descrizione });
								// }
								
							// }
						// }
				// );
				
				
				var data =  ManutenzioneFactory.getScenari(5).query(
					function() 
					{	
						
						 for(var i=0; i<data.length; i++) {						
							 data[i]["idData"] = i;

							
						 }	

						$scope.scenari = data;

					});
				
				
				$scope.$on('settingsUpdated', function (event, data) { 
					if(CreemSettings.selectedbuildings.length > 0)					
						createTable(CreemSettings.selectedbuildings[0].codice, "", "");
					if(CreemSettings.selectedClusters.length > 0)
						createTable("", CreemSettings.selectedClusters[0], "");
					if(CreemSettings.selectedCities.length > 0)
						createTable("", "", CreemSettings.selectedCities[0]);
				} );
       

		}],

  };
 
});
