CREEMapp.directive('buildingInfoImpianti', 

function() 
{
	
	return {
    controller: ['$scope', 'CreemSettings','PlantsFactory',
		
		function($scope, CreemSettings,PlantsFactory) 
		{			
			var illuminazioneInterna = 1;
			var riscaldamento = 2;
			var raffrescamento = 3;
			var illuminazioneEsterna = 5;
			
			var refreshData = function ()
			{										
					$scope.building = CreemSettings.selectedbuildings[0];	
					$scope.impianti = 
					{
						numCompIlluminazioneInterna: 0,
						illuminazioneInterna: "Illuminazione Interna",
						visibleIlluminazioneInterna: 1,
						numCompRiscaldamento: 0,
						riscaldamento: "Riscaldamento",
						visibleRiscaldamento: 1,
						numCompRaffrescamento: 0,
						raffrescamento: "Raffrescamento",
						visibleRaffrescamento: 1,
						numCompIlluminazioneEsterna: 0,
						illuminazioneEsterna: "Illuminazione Esterna",
						visibleIlluminazioneEsterna: 1
					};
					var impianti = PlantsFactory.getNumeroComponenti(CreemSettings.selectedbuildings[0].idImmobile).query(
					function()
					{
						for(var i=0; i<impianti.length; i++)
						{
							if(impianti[i].idTipoImp == illuminazioneInterna)
							{
								$scope.impianti.numCompIlluminazioneInterna = impianti[i].numeroComp;
								$scope.impianti.illuminazioneInterna = impianti[i].tipoImp;
							}
							else if(impianti[i].idTipoImp == riscaldamento)
							{
								$scope.impianti.numCompRiscaldamento = impianti[i].numeroComp;
								$scope.impianti.riscaldamento = impianti[i].tipoImp;
							}
							else if(impianti[i].idTipoImp == raffrescamento)
							{
								$scope.impianti.numCompRaffrescamento = impianti[i].numeroComp;
								$scope.impianti.raffrescamento = impianti[i].tipoImp;
							}
							else if(impianti[i].idTipoImp == illuminazioneInterna)
							{
								$scope.impianti.numCompIlluminazioneEsterna = impianti[i].numeroComp;
								$scope.impianti.illuminazioneEsterna = impianti[i].tipoImp;
							}
							
							$scope.impianti.potenzaInstallata = impianti[i].edificio.potenzaInstallata;
							$scope.impianti.denominazione = impianti[i].edificio.denominazione;
							$scope.impianti.indirizzo = impianti[i].edificio.indirizzo;
							$scope.impianti.nome = impianti[i].edificio.nome;
							$scope.impianti.siglaprov = impianti[i].edificio.siglaprov;
						}
						
					});
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


		}],
		
		templateUrl: 'directives/building-info-impianti.html'
  };
  
});
	
	