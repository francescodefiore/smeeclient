CREEMapp.directive('kpiAlertTable', 
function() 
{
	return {    
			templateUrl: 'directives/kpi-alert-table.html',
			
			controller: ['$scope', 'AlertFactory', 'ngTableParams', 'LoadingFactory', 'CreemSettings',
			
			function($scope, AlertFactory, ngTableParams, LoadingFactory, CreemSettings ) 
			{					
				var kpis = [];
				var filter_codice = "";
				var filter_cluster = "";
				var filter_prov = "";
				var filter_kpi = "";
				var filter_periodo = "";
						
				
				var m_data =  AlertFactory.getTipoKpi().query(
						function()
						{
							if (m_data.length > 0) {
								kpis.push(  {"id": "" , "title": "Tutti" } );
								for(var i=0; i<m_data.length; i++) {
									kpis.push({"id":m_data[i].descrizione, "title": m_data[i].descrizione });
								}
								
							}
						}
				);
				
				
				var createTable = function(cod, cluster, prov, kpi,periodo)
				{
					$scope.tableParams = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							 filter: {
								codice: cod,
								descrizione: kpi,
								siglaprov: prov,
								clusterEnergy: cluster,
								periodoRiferimento: periodo
							  }
							}, {
								data: data
					});		
					$scope.$broadcast('alertUpdated', data);					
				}
	
				var data =  AlertFactory.getALLBuildingAlert().query(
					function() 
					{	
						for(var i=0; i<data.length; i++)						
							data[i]["super"] = parseFloat(((parseFloat(data[i]["KPI"]).toFixed(2) - parseFloat(data[i]["Bench"]).toFixed(2))/parseFloat(data[i]["Bench"]).toFixed(2))*100).toFixed(2);
						
						if(CreemSettings.selectedbuildings.length > 0)					
							filter_codice = CreemSettings.selectedbuildings[0].codice;	
						
						createTable(filter_codice, filter_cluster, filter_prov, "",filter_periodo);
						
						
						
						
					});
				
				$scope.cols = [
					/*{ field: "codice", title: "Codice", filter: { codice: "text" }, show: true , sortable:"'codice'"},
					{ field: "clusterEnergy", title: "Cluster", filter: { clusterEnergy: "text" }, sortable:"'clusterEnergy'", show: true},
					{ field: "siglaprov", title: "Provincia", filter: { siglaprov: "text" }, sortable:"'siglaprov'", show: true},*/
					{ field: "descrizione", title: "KPI", filter: { descrizione: "select" }, filterData: kpis, show: true},
					{ field: "periodoRiferimento", title: "Periodo Riferimento", filter: { periodoRiferimento: "text" }, sortable:"'periodoRiferimento'", show: true},					
					{ field: "KPI", title: "Valore KPI" },
					{ field: "Bench", title: "Benchmark"},
					{ field: "super", title: "Performance (%)"}
					
					
					
				];
				
				$scope.$on('settingsUpdated', function (event, data) { 
					
					if(CreemSettings.selectedbuildings.length > 0)					
						createTable(CreemSettings.selectedbuildings[0].codice, "", "", "");
					if(CreemSettings.selectedClusters.length > 0)
						createTable("", CreemSettings.selectedClusters[0], "", "");
					if(CreemSettings.selectedCities.length > 0)
						createTable("", "", CreemSettings.selectedCities[0], "");
				} );

		}],

  };
 
});
