CREEMapp.directive('acotelChart', 

function() 
{

	return {
    scope: {},
    controller: ['$rootScope', '$scope', 'CreemSettings', 'ConsumptionsFactory', 'WeatherFactory', 'LocalWeatherFactory',
	
		function($rootScope, $scope, CreemSettings, ConsumptionsFactory, WeatherFactory, LocalWeatherFactory)
		{			
			var weatherValue = '';
			var seriesCounter = 0;
			$scope.dataset = [];
			$scope.dataconsumptions = {};
			var histconsumptionschart;			
			
			var createChart = function(){
				histconsumptionschart = new Highcharts.Chart({
					chart: {
						renderTo: 'acotelconsumptions',
						type: 'spline',
						zoomType: 'x'
					},
					title: {
						text: 'Consumi(kWh)'
					},
					
					credits: {
						enabled: false
					},
					
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: { // don't display the dummy year
							month: '%e. %b',
							year: '%b'
						},
						title: {
							text: 'Date'
						}
					},
					yAxis: [{ // Primary yAxis
                        labels: {
                        	format: '{value} kWh',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        title: {
                        	text: 'Consumi (kWh)',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    },  { // Secondary yAxis
                        title: {
                       		text: weatherValue,
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        labels: {
                        	format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        opposite: true
                    }],

					plotOptions: {
						spline: {
							marker: {
								enabled: false
							}
						}
					},

					series: $scope.dataset
				});
			}

			var getdata = function (building, datefrom, dateto) {

				$scope.dataconsumptions[building.codice] = ConsumptionsFactory.getDataAcotel(building.codice, datefrom, dateto).query(
					function() {
						if($scope.dataconsumptions[building.codice].length) {
							$scope.dataset.push({
								name: building.codice,
								data: prepareConsumptionsData($scope.dataconsumptions[building.codice]),
								yAxis: 0,
							});

							
							seriesCounter += 1;
                                if (seriesCounter === CreemSettings.selectedbuildingsAcotel.length) {
                                	createChart();
                                }
								
						}
					}
				);
				
			}
			
			var prepareConsumptionsData = function (response, weather)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
					var consumi = [response[i]['01_am'], response[i]['02_am'], response[i]['03_am'], response[i]['04_am'], response[i]['05_am'],
						response[i]['06_am'], response[i]['07_am'], response[i]['08_am'], response[i]['09_am'], response[i]['10_am'], response[i]['11_am'],response[i]['12_am'],
						response[i]['01_pm'], response[i]['02_pm'], response[i]['03_pm'], response[i]['04_pm'], response[i]['05_pm'], response[i]['06_pm'],
						response[i]['07_pm'], response[i]['08_pm'], response[i]['09_pm'], response[i]['10_pm'], response[i]['11_pm'], response[i]['12_pm']];
					for (var j = 0; j < 24; j++) {
						var newDate = new Date(response[i]['data']);						
						chartData.push([
							Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), j, 0, 0, 0),
							consumi[j]
						]);

					}
				}				
				return chartData;
			}
			
			
			
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);
					$scope.dataconsumptions = {}
					seriesCounter = 0;
							
										
					for(var i=0; i<CreemSettings.selectedbuildingsAcotel.length; i++)
					{							
							getdata(CreemSettings.selectedbuildingsAcotel[i], CreemSettings.selectedDatesAcotel.from, CreemSettings.selectedDatesAcotel.to);
					}
					
					var index = [];
					var ctr = 0;
					if(histconsumptionschart != null)
					{
						for(var i=0;i<histconsumptionschart.series.length;i++)
						{
							var find = [];
							for(var y=0; y<CreemSettings.selectedbuildingsAcotel.length; y++)
							{
								find = $.grep(CreemSettings.selectedbuildingsAcotel[y], function(item) {
									return item.codice == histconsumptionschart.series[i].name;
								});
								
								if(find != '')
									break;
							}
							
							if(find == '')
							{
								index[ctr] = i;
								ctr++;
							}
							
						}
						
						for(var i=index.length-1;i>=0;i--)
							histconsumptionschart.series[index[i]].remove(true);
					}
					
			}
			
			$scope.$on('settingsUpdatedAcotel', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildingsAcotel.length > 0)
				refreshData();


			
		}],
		
		templateUrl: 'directives/acotel-chart.html'
  };
  
});
	
	