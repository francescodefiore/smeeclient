CREEMapp.directive('co2SavedChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'ManutenzioneFactory',
	
		function($scope, CreemSettings, ManutenzioneFactory)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			
			var createChart = function(title)
			{
				var co2risparmiata = new Highcharts.Chart({
				chart: {
						renderTo: 'co2risparmiata',
						type: 'column'
					},
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					xAxis: {
						categories: [
							'Gen',
							'Feb',
							'Mar',
							'Apr',
							'Mag',
							'Giu',
							'Lug',
							'Ago',
							'Set',
							'Ott',
							'Nov',
							'Dic'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'CO2 Risparmiata (kg)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}:</td> <td>{point.y} Kg</td> </tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0,
							stacking: 'value'
						} 
					},
					legend: {
						enabled: false
					},
					series: $scope.dataset
				});
			}
							

			var getdata = function (anno) 
			{
				var data = [];
					
				data = ManutenzioneFactory.getCO2(anno).query( function() { 
					var co2=[];
					for(var i=0; i<data.length; i++)
					{
						co2[i]=data[i].kgCO2Risparmiati;							
					}
					$scope.dataset = [
					{
						name: 'Co2 Risparmiata',
						data: co2
											
					}];
					createChart('CO2 Risparmiata Gestione Ticket anno ' + anno);	
				});								
			}
						
			
			var refreshData = function ()
			{	
				var anno = moment().subtract(1,'years').format("YYYY");
				//getdata(anno);
				getdata(2014);
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			//if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
   

		}],
		
		templateUrl:'directives/co2-saved-chart.html', 
		scope: true
  };
  
});