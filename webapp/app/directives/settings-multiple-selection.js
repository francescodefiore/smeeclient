CREEMapp.directive('settingsMultipleSelection', function() {
	
	  return {
		scope: {},
	  
		templateUrl: 'directives/settings-multiple-selection.html',
				
		controller: ['$scope', 'BuildingsFactory', 'CreemSettings',   function($scope, BuildingsFactory, CreemSettings) {
									
									
							$scope.selectedBuildings = [];
							
							$scope.selected_buildings = [];

							$scope.selected_weather = '';
							
							$scope.immobili = BuildingsFactory.getBuildings().query(
								function(){
									
									if(CreemSettings.selectedbuildings.length == 0)
									{
										//CreemSettings.setBuilding($scope.immobili[0]);
										for(var i=0;i<$scope.immobili.length;i++)
										{//facciamo partire con PAX1400
											if($scope.immobili[i]['idImmobile'] == 10)
											{
												CreemSettings.setBuilding($scope.immobili[i]);
												break;
											}
										}
									}
									
									for(var i=0; i<CreemSettings.selectedbuildings.length; i++)
									{									
										$scope.selected_buildings.push( CreemSettings.selectedbuildings[i].codice);										
										$scope.selectedBuildings = CreemSettings.selectedbuildings;
									}
									
									
								}
							);
							

							$scope.selectedDate = CreemSettings.selectedDates; 
							$scope.selected_clusters = CreemSettings.selectedClusters;
							
							$scope.selected_dates = {startDate: moment(CreemSettings.selectedDates.from), endDate: moment(CreemSettings.selectedDates.to)}
							
							$scope.ranges = {							
								// CREEM: L'anno corrente è il precedente								
								'Ieri': [moment().subtract(1,'year').subtract(1, 'days'), moment().subtract(1,'year').subtract(1, 'days')],
								'Ultimi 7 giorni': [moment().subtract(1,'year').subtract(7, 'days'), moment().subtract(1,'year')],
								'Ultimi 30 giorni': [moment().subtract(1,'year').subtract(30, 'days'), moment().subtract(1,'year')],
								'Mese corrente': [moment().subtract(1,'year').startOf('month'), moment().subtract(1,'year').subtract(1, 'days')]
							};
														
							
							$scope.setFilters = function() {
																								
								var selDates = {from:  moment($scope.selected_dates.startDate).format("YYYY-MM-DD"), to: moment($scope.selected_dates.endDate).format("YYYY-MM-DD")};															
								var conf = { buildings: $scope.selectedBuildings, clusters: $scope.selected_clusters, dates: selDates, weather: $scope.selected_weather};
								CreemSettings.setSettings(conf);
							}
														
						}
		],
		
		link: function(scope, element)
			  { 			  			
					$(element).find('#clusters_list').val(scope.selected_clusters);	
					$(element).find('#date_range').val(scope.selectedDate.from + " - " + scope.selectedDate.to);
					$(element).find('#buildings_list').val(scope.selected_buildings);
					
					scope.$watch('immobili', function(newValue, oldValue) {																	
																				
										if (newValue.length != 0)																				
											$(element).find('.chosen-select').chosen();
										
								}, true);
					
				}
		
		
	  }
	});
	
	