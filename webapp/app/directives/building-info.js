CREEMapp.directive('buildingInfo', 

function() 
{
	
	return {
    controller: ['$scope', 'CreemSettings',
		
		function($scope, CreemSettings) 
		{			
							
			var refreshData = function ()
			{										
					$scope.building = CreemSettings.selectedbuildings[0];									
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


		}],
		
		templateUrl: 'directives/building-info.html'
  };
  
});
	
	