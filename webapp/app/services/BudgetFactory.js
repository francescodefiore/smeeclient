
CREEMapp.factory('BudgetFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/Budget';
			var BudgetFactory = {};

			
			BudgetFactory.getBuildingBudget = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			BudgetFactory.getBuildingBudgetYear = function(pod,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?filterBy=POD&filterParams=" + pod + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			BudgetFactory.getCityBudget = function(city) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?groupBy=PROV&groupByParams=" + city, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			BudgetFactory.getCityBudgetYear = function(city,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?groupBy=PROV&groupByParams=" + city + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			BudgetFactory.getClusterBudget = function(cluster) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?groupBy=CLUSTERENERGY&groupByParams=" + cluster, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			BudgetFactory.getClusterBudgetYear = function(cluster,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?groupBy=CLUSTERENERGY&groupByParams=" + cluster + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			return BudgetFactory;
	}
]);


