CREEMapp.service('TooltipTicketService', ['$uibModal',
    function ($uibModal) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'directives/tooltipticket.html'
        };
		
		this.showModal = function (customModalDefaults, customModalOptions) {
           
			var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'directives/tooltipticket.html',
			controller: function ($scope, $uibModalInstance) {
			
							var tempModalOptions = {};
							$scope.ok = function () {
								$uibModalInstance.close();
							};

							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
							  
							var modalOptions = 
							{
								actionButtonText: 'OK',
								headerText: 'Ticket',
								responsabile: 'Perform this action?',
								utenteInserimento: '',
								immobile:'',
								descrizione: 'Perform this action?',
								dataInizioIntervento:'',
								dataLimite:'',
								durata:''
							};
							
							angular.extend(tempModalOptions, $scope.modalOptions, customModalOptions);
							$scope.modalOptions = tempModalOptions;
						},
			//size: size,
			/*resolve: {
			items: function () {
				  return $scope.items;
				}
			  }*/
			});

			modalInstance.result.then(function () {
			  //$cookies.putObject('building',CreemSettings.selectedbuildings[0]);
			});
        };
    }]);