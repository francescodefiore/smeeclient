CREEMapp.factory('ManutenzioneFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 
	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{
		var urlBase = '/manutenzione';
		var ManutenzioneFactory = {};
		
		ManutenzioneFactory.getBuildingInterventi = function(idImmobile) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/interventi?idImmobile=" + idImmobile, {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		
		ManutenzioneFactory.getALLBuildingsInterventi = function() 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/interventi/all", {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: false,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getTipoIntervento = function() 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/getinterventi", {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getScenari = function(scenari) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/scenari?scenari=" + scenari, {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getScenariManutentivi = function(idImmobile) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + "/dss/scenari?idImmobile=" + idImmobile, {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getTicket = function()  
		{	
			LoadingFactory.startLoading();
			var time = moment().subtract(1,'years').format("YYYY-MM-DD");
			return $resource(RESTservAddr + urlBase + "/ticket?time=" + time , {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: false,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getTicketDaSchedulare = function()  
		{	
			LoadingFactory.startLoading();
			var time = moment().subtract(1,'years').format("YYYY-MM-DD");
			return $resource(RESTservAddr + urlBase + "/ticketDaSchedulare?time=" + time , {},
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: false,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.getCO2 = function(anno) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/ticketCO2?anno=" + anno , {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		ManutenzioneFactory.insertTicket = function(tickets)
		{
			LoadingFactory.startLoading();
			//var parameter = JSON.stringify(tickets);
				$http.post(RESTservAddr + urlBase + "/insertTickets", tickets).
				success(function(data, status, headers, config) {
					LoadingFactory.stopLoading();
					console.log(data);
				  }).
				  error(function(data, status, headers, config) {
					LoadingFactory.stopLoading();
				  });
		}
		
		ManutenzioneFactory.getCO2Manutenzione = function(annoDa,annoA) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/co2Manutenzione?annoDa=" + annoDa + "&annoA="+annoA , {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		
				
		return ManutenzioneFactory;
	}
]);