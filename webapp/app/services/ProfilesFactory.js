
CREEMapp.factory('ProfilesFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/profili';
			var ProfilesFactory = {};

			
			ProfilesFactory.getBuildingBaseload = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + "/baseloads" + "?filterBy=POD&filterParams=" + pod, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			ProfilesFactory.getBuildingProfilesMonFri = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod + "&filterBy=GROUP&filterParams=pod&filterBy=TURNO&filterParams=lunven", {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			ProfilesFactory.getBuildingProfilesSaturday = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod + "&filterBy=GROUP&filterParams=pod&filterBy=TURNO&filterParams=sabato", {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			ProfilesFactory.getBuildingProfilesHoliday = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod + "&filterBy=GROUP&filterParams=pod&filterBy=TURNO&filterParams=festivi", {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			
			
			ProfilesFactory.getClusterProfilesMonFri = function(cluster, zone, time) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=CLUSTER&filterParams=" + cluster + "&filterBy=GROUP&filterParams=cluster&filterBy=TURNO&filterParams=lunven&filterBy=ORARIO&filterParams=" + time + "&filterBy=ZONA&filterParams=" + zone, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			ProfilesFactory.getClusterProfilesSaturday = function(cluster, zone, time) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=CLUSTER&filterParams=" + cluster + "&filterBy=GROUP&filterParams=cluster&filterBy=TURNO&filterParams=sabato&filterBy=ORARIO&filterParams=" + time + "&filterBy=ZONA&filterParams=" + zone, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			ProfilesFactory.getClusterProfilesHoliday = function(cluster, zone) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=CLUSTER&filterParams=" + cluster + "&filterBy=GROUP&filterParams=cluster&filterBy=TURNO&filterParams=festivi&filterBy=ZONA&filterParams=" + zone, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			
			
			return ProfilesFactory;
	}
]);


