
CREEMapp.factory('MonthlyConsumptionsBandsFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/multiorarie';
			var MonthlyConsumptionsBandsFactory = {};

			
			MonthlyConsumptionsBandsFactory.getBuildingConsumptions = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/podf1f2f3?filterBy=PODF1F2F3&filterParams=" + pod, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsBandsFactory.getBuildingConsumptionsYear = function(pod,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/podf1f2f3/anno?filterBy=PODF1F2F3&filterParams=" + pod + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsBandsFactory.getCityConsumptions = function(city) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/provincef1f2f3?filterBy=PROVF1F2F3&groupByParams=" + city, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsBandsFactory.getCityConsumptionsYear = function(city,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/provincef1f2f3/anno?filterBy=PROVF1F2F3&groupByParams=" + city + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsBandsFactory.getClusterConsumptions = function(cluster) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/clusterf1f2f3?filterBy=CLUSTERF1F2F3&groupByParams=" + cluster, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsBandsFactory.getClusterConsumptionsYear = function(cluster,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/clusterf1f2f3/anno?filterBy=CLUSTERF1F2F3&groupByParams=" + cluster + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			
			return MonthlyConsumptionsBandsFactory;
	}
]);


