
CREEMapp.factory('BuildingsFactory', ['RESTservAddr','$resource','LoadingFactory', 


	function(RESTservAddr, $resource, LoadingFactory) 
	{

		var urlBase = '/immobili?filterBy=CREEM&filterParams=SI';
		var urlBaseWithoutFilter = '/immobili';
		var BuildingsFactory = {};

		
		BuildingsFactory.getBuildings = function() {

			LoadingFactory.startLoading();
			

			return $resource(RESTservAddr + urlBase, {}, {
								query: {
									method: 'GET',
									isArray : true,
									cache: true,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
		}
		
		BuildingsFactory.getBuildingsAcotel = function() {

			LoadingFactory.startLoading();
			

			return $resource(RESTservAddr + urlBaseWithoutFilter + '/acotel', {}, {
								query: {
									method: 'GET',
									isArray : true,
									cache: true,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
		}

		BuildingsFactory.getBuilding = function(idImmobile) {

			LoadingFactory.startLoading();
			
			return $resource(RESTservAddr + "/immobile?idImmobile=" + idImmobile, {}, {
								query: {
									method: 'GET',
									isArray : false,
									cache: false,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
			}		


		return BuildingsFactory;
	}
]);



CREEMapp.factory("BuildingForm", function(RESTservAddr, $resource)
		{			
			var urlBase = RESTservAddr + '/immobile';
			return $resource(urlBase, {}, {
				update: {
					method: 'POST'
				}
			});
		}
);


