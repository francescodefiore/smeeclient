CREEMapp.service('TooltipScenarioService', ['$uibModal','ManutenzioneFactory','CreemSettings',
    function ($uibModal,ManutenzioneFactory,CreemSettings) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'directives/tooltipscenario.html'
        };

        var modalOptions = {
            actionButtonText: 'Genera Ticket Intervento',
			closeButtonText: 'Annulla'            
        };
		
		var customModalOptions;

        this.showModal = function (customModalDefaults, customModalOptions) {
           
			var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'directives/tooltipscenario.html',
			controller: function ($scope, $uibModalInstance) {
			
							var tempModalOptions = {};
							$scope.ok = function () {
								$uibModalInstance.close();
							};

							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
							
							$scope.generaTicket = function(result)
							{
								var tickets = createJson(customModalOptions["interfventiIll"]).
											  concat(createJson(customModalOptions["interventiClima"])).
											  concat(createJson(customModalOptions["interventiOpaco"])).
											  concat(createJson(customModalOptions["interventiTrasp"]));
								
								ManutenzioneFactory.insertTicket(tickets);
								$uibModalInstance.close();
							}
							  
							$scope.modalOptions = {
							actionButtonText: 'Genera Ticket Intervento',
							closeButtonText: 'Annulla'            
							};
							
							angular.extend(tempModalOptions, $scope.modalOptions, customModalOptions);
							$scope.modalOptions = tempModalOptions;
							
							var createJson = function(vet)
							{
								var tickets = [];
								for(var i=0;i<vet.length;i++)
								{
									var ticket = {
									"idImmobile": vet[i].idImmobile,
									"codiceImmobile" : vet[i].codiceImmobile,
									"denominazioneUp" : vet[i].ubicazione,
									"descrizione" : vet[i].numeroInterventi + " interventi. " + vet[i].descrizioneIntervento.replace("'","''"),
									"dataRichiesta" : setDate(),
									"categoria" : 5,
									"tipoTicket" : "MS",
									"idImpComp" : -1,
									"co2Risparmiata" : vet[i].epiSimulato*CreemSettings.selectedbuildings[0].superficieUtile*0.41/1000,
									"priorita" : 0,
									"provincia" : setProvincia(vet[i].codiceImmobile),
									"prioritaStr" : "STRAORDINARIA"
									};
								
								tickets.push(ticket);
								}
								
								return tickets;
							}
		
							var setProvincia = function(codiceImmobile)
							{
								var provincia = codiceImmobile.substr(0,2);
								var nPro;
								switch(provincia) {
									case 'AG' : nPro=  1000; break;
									case 'CL' : nPro=  1001; break;
									case 'CT' : nPro=  1002; break;
									case 'EN' : nPro=  1003; break;
									case 'ME' : nPro=  1004; break;
									case 'PA' : nPro=  1005; break;
									case 'RG' : nPro=  1006; break;
									case 'SR' : nPro=  1007; break;
									case 'TP' : nPro=  1008; break;
									default : nPro= 1000
								}
								
								return nPro;
							}
		
							var setDate = function()
							{
								var today = new Date();
								var dd = today.getDate();
								var mm = today.getMonth()+1; //January is 0!

								var yyyy = today.getFullYear();
								if(dd<10){
									dd='0'+dd
								} 
								if(mm<10){
									mm='0'+mm
								} 
								var today = yyyy+'-'+mm+'-'+dd;
								return today;
							}
						},
			//size: size,
			/*resolve: {
			items: function () {
				  return $scope.items;
				}
			  }*/
			});

			modalInstance.result.then(function () {
			  //$cookies.putObject('building',CreemSettings.selectedbuildings[0]);
			});
        };

    }]);