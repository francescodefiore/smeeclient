CREEMapp.factory("CreemSettings", ['$rootScope', function ($rootScope) {
    
	var settingsObject;

	
    settingsObject = {
	
        selectedbuildings: [],
		
		selectedbuildingsAcotel: [],

        selectedClusters: [],
		
		selectedCities: [],
		
		numInstances: [],

		selectedWeather: '',
		
		selectedMeasures: '',
		
		selectedSuperficieDss: '',
		
        selectedDates: { from: moment().subtract(1,'year').subtract(7, 'days').format("YYYY-MM-DD"), to: moment().subtract(1,'year').format("YYYY-MM-DD") },
		
		selectedDatesAcotel: { from: moment().subtract(7, 'days').format("YYYY-MM-DD"), to: moment().format("YYYY-MM-DD") },
		
		selectedDatesCampo: { from: moment().subtract(7, 'days').format("YYYY-MM-DD"), to: moment().format("YYYY-MM-DD") },
		
		selectedYear: '',
		
		reset: function()
		{
			this.selectedbuildings = [];
            this.selectedClusters = [];         
			this.selectedCities = [];
			this.selectedMeasures= '';
			this.selectedSuperficieDss = '';
			this.selectedDates.from = moment().subtract(1,'year').subtract(7, 'days').format("YYYY-MM-DD");
			this.selectedDates.to = moment().subtract(1,'year').format("YYYY-MM-DD");
			//this.selectedDatesCampo.from = moment().subtract(7, 'days').format("YYYY-MM-DD");
			//this.selectedDatesCampo.to = moment().format("YYYY-MM-DD");
		},
		
		resetAcotel : function()
		{
			this.selectedbuildingsAcotel = [];
			this.selectedDatesAcotel.from = moment().subtract(7, 'days').format("YYYY-MM-DD");
			this.selectedDatesAcotel.to = moment().format("YYYY-MM-DD");
		},
		
        setSettings: function (conf) 
		{
			this.reset();
            this.selectedbuildings = conf.buildings;
            this.selectedClusters = conf.clusters;         
            this.selectedDates = conf.dates;
			this.selectedDatesCampo = conf.datescampo;
			this.selectedWeather = conf.weather;
			this.selectedMeasures= conf.measures;
			this.selectedSuperficieDss = conf.selectedSuperficieDss;
			this.notifyObservers();
			
			
        },
		
		setSettingsAcotel: function (conf)
		{
			//Per Acotel
			this.resetAcotel();
			this.selectedbuildingsAcotel = conf.buildingsAcotel;
			this.selectedDatesAcotel = conf.dates;
			this.notifyObserversAcotel();
		},
		
		setBuilding: function(building)
		{
			this.reset();
			this.selectedbuildings.push(building);			
			this.notifyObservers();
		},
		
		setBuildingAcotel: function(building)
		{
			this.resetAcotel();
			this.selectedbuildingsAcotel.push(building);
			this.notifyObserversAcotel();
		},
		
		setCity: function(city)
		{
			this.reset();
			this.selectedCities[0] = city;			
			this.notifyObservers();
		},
		
		setCluster: function(cluster)
		{
			this.reset();		
			this.selectedClusters[0] = cluster;			
			this.notifyObservers();
		},
		
		setMeasure: function(measure)
		{
			this.reset();		
			this.selectedMeasures = measure;			
			this.notifyObservers();
		},
		
		setSuperficieDss: function(supdss)
		{
			this.reset();		
			this.selectedSuperficieDss = supdss;			
			this.notifyObservers();
		},
		
		setBoards: function(numInstances)
		{
			if(this.numInstances.length == (numInstances == 0 ? 0 : numInstances-1))
			{
				this.numInstances = [];
				this.notifyObserverBoards();
			}
			else
				this.numInstances.push(1);
		},
		
		setYear: function(year)
		{
			this.selectedYear = year;
		},
		
		observerCallbacks: [],
			
		notifyObservers: function(){
			
			$rootScope.$broadcast('settingsUpdated', {});
				
		},
		
		notifyObserversAcotel: function(){
			
			$rootScope.$broadcast('settingsUpdatedAcotel', {});
				
		},
		
		notifyObserverBoards: function(){
			
			$rootScope.$broadcast('setBoards', {});
				
		}

    };
	
	//settingsObject.reset();
	
    return settingsObject;
}]);