CREEMapp.factory('AlertFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 
	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{
		var urlBase = '/kpi';
		var AlertFactory = {};
		
		AlertFactory.getBuildingAlerts = function(idImmobile) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + "/alerts?filterBy=ALERTBUILD&filterParams=" + idImmobile, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();
							},
							responseError: function (data) {
								LoadingFactory.stopLoading();
							}
						}							
					}							
				});
		}
		
		AlertFactory.getALLBuildingAlert = function() 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/alert", {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();
							},
							responseError: function (data) {
								LoadingFactory.stopLoading();
							}
						}							
					}							
				});
		}

	
		AlertFactory.getTipoKpi = function() 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "/gettipo", {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();
							},
							responseError: function (data) {
								LoadingFactory.stopLoading();
							}
						}							
					}							
				});
		}

		
		AlertFactory.getForClusterAlert = function(cluster) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + "/alerts?filterBy=CLUSTER&filterParams=" + cluster, {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		AlertFactory.getForProvinciaAlert = function(prov) 
		{	
			LoadingFactory.startLoading();
			return $resource(RESTservAddr + "/alerts?filterBy=SIGLAPROV&filterParams=" + prov, {}, 
			{
				query: {
					method: 'GET',
					isArray : true,
					cache: true,										
					interceptor: {
						response: function (data) {										
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							LoadingFactory.stopLoading();
						}
					}							
				}							
			});
		}
		
		return AlertFactory;
	}
]);
