CREEMapp.factory('ConsumptionsFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/multiorarie';
			var ConsumptionsFactory = {};

			
			ConsumptionsFactory.getConsumptions = function(pod, datefrom, dateto) 
			{

				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod + "&filterBy=TIMEINTERVAL&filterParams=" + datefrom + ";" + dateto, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();												
											},
											responseError: function (data) {
												console.log("error");
												LoadingFactory.stopLoading();
											}
										}					
									}							
								});
								
			}
			
			ConsumptionsFactory.getDataAcotel = function(codiceImmobile, datefrom, dateto) 
			{

				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/acotel?filterBy=CODICEIMM&filterParams=" + codiceImmobile + "&filterBy=TIMEINTERVAL&filterParams=" + datefrom + ";" + dateto, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();												
											},
											responseError: function (data) {
												console.log("error");
												LoadingFactory.stopLoading();
											}
										}					
									}							
								});
								
			}
			
			
			
			return ConsumptionsFactory;
	}
]);




