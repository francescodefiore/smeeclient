CREEMapp.service('ModalService', ['$uibModal',
    function ($uibModal) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'directives/epi-form/modal.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
           
			var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'directives/epi-form/modal.html',
			controller: function ($scope, $uibModalInstance) {
			
							var tempModalOptions = {};
							$scope.ok = function () {
								$uibModalInstance.close();
							};

							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
							  
							$scope.modalOptions = {
								closeButtonText: 'Close',
								actionButtonText: 'OK',
								headerText: 'Proceed?',
								bodyText: 'Perform this action?'
							};
							
							angular.extend(tempModalOptions, $scope.modalOptions, customModalOptions);
							$scope.modalOptions = tempModalOptions;
						},
			//size: size,
			/*resolve: {
			items: function () {
				  return $scope.items;
				}
			  }*/
			});

			/*modalInstance.result.then(function () {
			  //$cookies.putObject('building',CreemSettings.selectedbuildings[0]);
			});*/
			
			return modalInstance.result;
        };

    }]);