CREEMapp.factory('LocalWeatherFactory', ['RESTservAddr','$resource','LoadingFactory','$http',

    function(RESTservAddr, $resource, LoadingFactory, $http) {
        var LocalWeatherFactory = {};
        var urlBase = '/weather';

        var fixDate = function(str) {
            var mdy = str.split('-');
            var date = new Date(mdy[0], mdy[1]-1, mdy[2]);
            date.setDate(date.getDate()+1);

            var month = '' + (date.getMonth() + 1),
                day = '' + date.getDate(),
                year = date.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        LocalWeatherFactory.getWeather = function(city, dateFrom, dateTo) {
        	dateTo = fixDate(dateTo);

        	LoadingFactory.startLoading();
        	return $resource(RESTservAddr + urlBase + "?city=" + city + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo, {}, {
        		query: {
        			method: 'GET',
        			isArray : true,
        			cache: true,
        			interceptor: {
        				response: function (data) {
        					LoadingFactory.stopLoading();
        				},
        				responseError: function (data) {
        					LoadingFactory.stopLoading();
        				}
        			}
        		}
        	});
        }

        return LocalWeatherFactory;
    }
])