'use strict';

CREEMapp.controller('MapController', ['$rootScope', '$scope', 'leafletData', 'BuildingsFactory', 'CreemSettings',
		function ($rootScope, $scope, leafletData, BuildingsFactory, CreemSettings) 
		{

			$scope.default_values = {
							scrollWheelZoom: false
			};
			
			$scope.center = {};


			var refreshData = function ()
			{				

				$scope.markers = [];
				$scope.immobili = [];
				
				$scope.building = {};
				$scope.immobili = BuildingsFactory.getBuildings().query(function(){
				
						$scope.building = CreemSettings.selectedbuildings[0];
						
						$scope.center = {
									lat: CreemSettings.selectedbuildings[0].latitudine,
									lng: CreemSettings.selectedbuildings[0].longitudine,
									zoom: 10
								};
								
						
								
						
						

						var len = $scope.immobili.length;
						for (var i=0; i<len; i++) {
							if ($scope.building.pod != $scope.immobili[i].pod) {
								$scope.markers.push({
									lat: $scope.immobili[i]["latitudine"],
									lng: $scope.immobili[i]["longitudine"],
									message: $scope.immobili[i]["codice"],
									focus: false,
									icon: {},
									draggable: false,
									building: $scope.immobili[i]
								});
							}
							else
							{
								$scope.markers.push({
									lat: $scope.immobili[i]["latitudine"],
									lng: $scope.immobili[i]["longitudine"],
									message: $scope.immobili[i]["denominazione"] + "<br>" + $scope.immobili[i]["indirizzo"] + ", " + $scope.immobili[i]["nome"] + " (" + $scope.immobili[i]["siglaprov"] + ")",
									focus: true,
									draggable: false,
									icon: {
										iconUrl: "styles/images/marker.png",
										iconSize: [64,64],
										iconAnchor: [32, 64],
										popupAnchor: [0, -65]
									},
									building: $scope.immobili[i]
								});
							
							}

						}
						
						
								
				 });
				
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
					
			if(CreemSettings.selectedbuildings.length > 0)
					refreshData();
					

		
				$scope.$on('leafletDirectiveMarker.click', function(e, args) {
					console.log(args.model.building);					
					CreemSettings.setBuilding(args.model.building);
					refreshData();
				});
		}
]);