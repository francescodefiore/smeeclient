/* Controllers */

CREEMapp.controller('MainController', ['$scope', '$translate', '$localStorage', '$window', 'LoginService', '$location',
    function(              $scope,   $translate,   $localStorage,   $window,  LoginService, $location ) {
      // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');	  
      $scope.$location = $location;
      $scope.getUsername = function () {
        return LoginService.getUsername();
      };
      // config
      $scope.app = {
        name: 'creem',
        version: '0.0.1',
        // for chart colors
        color: {
          primary: '#7266ba',
          info:    '#23b7e5',
          success: '#27c24c',
          warning: '#fad733',
          danger:  '#f05050',
          light:   '#e8eff0',
          dark:    '#3a3f51',
          black:   '#1c2b36'
        },
        settings: {
          themeID: 2,
          navbarHeaderColor: 'bg-success',
          navbarCollapseColor: 'bg-success',
          asideColor: 'bg-black',
          headerFixed: false,
          asideFixed: false,
          asideFolded: true,
          asideDock: true,
          container: true
        }		
      }

      // save settings to local storage
      if ( angular.isDefined($localStorage.settings) ) {
        $scope.app.settings = $localStorage.settings;
      } else {
        $localStorage.settings = $scope.app.settings;
      }
      $scope.$watch('app.settings', function(){
        if( $scope.app.settings.asideDock  &&  $scope.app.settings.asideFixed ){
          // aside dock and fixed must set the header fixed.
          $scope.app.settings.headerFixed = true;
        }
        // save to local storage
        $localStorage.settings = $scope.app.settings;
      }, true);

      // angular translate
      $scope.lang = { isopen: false };
      $scope.langs = {en:'English', it:'Italian'};
      $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "Italian";
      $scope.setLang = function(langKey, $event) {
        // set the current lang
        $scope.selectLang = $scope.langs[langKey];
        // You can change the language during runtime
        $translate.use(langKey);
        $scope.lang.isopen = !$scope.lang.isopen;
      };

      //flag for hiding/showing menu when user is not signed/signed
      $scope.userLoggedIn = true;

      $scope.$on('loginSuccessfulEvent', function () {
        $scope.userLoggedIn = true;
      });
      $scope.$on('logoutSuccessfulEvent', function () {
        $scope.userLoggedIn = false;
      });

      function isSmartDevice( $window )
      {
          // Adapted from http://www.detectmobilebrowsers.com
          var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
          // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
          return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
      }

  }]);
