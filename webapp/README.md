# CREEM Project - AngularJS client application 

## Authors

	Davide De Tommaso (dtmdvd@gmail.com)

## 1. Requirements
	
	# 1.1 Node.js
	
	# 1.2 Bower
	
		$ npm install -g bower
		
	# 1.3 Karma (optional)
	
		$ npm install karma --save-dev
		
	# 1.4 Gulp
	
		$ npm install --save-dev gulp
	
## 2. Running

	$ cd app; npm start
	
## 3. Building

	$ gulp; cd public; npm start

